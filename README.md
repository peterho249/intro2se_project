# README #

# Student Management Project #

## Group Information ##
* Name: DragonSlayerV
* Member:

1. Ho Xuan Dung
2. Lai Ngoc Bao
3. Tran Manh Chung
4. Duong Trung Duy
5. Nguyen Tan Dat

## Application Info ##
* This is a student management application written in C# and SQL server

## How to build ##
* First of all, you have to execute database in StudentMan.sql on Sql Server 2008 Express.
* Then build application source on Visual Studio 2017 to get executable file.

## Sample account ##
### Admin account ###
* Username: admin
* Password: admin

### User account ###
* Username: 1512055
* Password: 1512055

## Note ##
* If the database connection error occur, please check whether there is StudentMan database in Sql Server.
* If database is already and error still occur, please correct the connection string in file DAO/DataProcider.cs for suitable for your system.