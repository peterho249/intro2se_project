﻿namespace StudentManagementApp
{
    partial class fViewPoint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.idStudentTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chooseYearCombobox = new System.Windows.Forms.ComboBox();
            this.chooseSemesterCombobox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.changePointButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pointDatagrid = new System.Windows.Forms.DataGridView();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pointDatagrid)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.idStudentTextbox);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.chooseYearCombobox);
            this.panel2.Controls.Add(this.chooseSemesterCombobox);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.changePointButton);
            this.panel2.Controls.Add(this.backButton);
            this.panel2.Controls.Add(this.okButton);
            this.panel2.Location = new System.Drawing.Point(7, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(194, 317);
            this.panel2.TabIndex = 5;
            // 
            // idStudentTextbox
            // 
            this.idStudentTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idStudentTextbox.Location = new System.Drawing.Point(22, 34);
            this.idStudentTextbox.Name = "idStudentTextbox";
            this.idStudentTextbox.Size = new System.Drawing.Size(153, 26);
            this.idStudentTextbox.TabIndex = 13;
            this.idStudentTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.idStudentTextbox.TextChanged += new System.EventHandler(this.idStudentTextbox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(43, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 18);
            this.label2.TabIndex = 12;
            this.label2.Text = "Chọn sinh viên";
            // 
            // chooseYearCombobox
            // 
            this.chooseYearCombobox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chooseYearCombobox.FormattingEnabled = true;
            this.chooseYearCombobox.Items.AddRange(new object[] {
            "Tất cả"});
            this.chooseYearCombobox.Location = new System.Drawing.Point(22, 94);
            this.chooseYearCombobox.Name = "chooseYearCombobox";
            this.chooseYearCombobox.Size = new System.Drawing.Size(110, 26);
            this.chooseYearCombobox.TabIndex = 11;
            this.chooseYearCombobox.SelectedIndexChanged += new System.EventHandler(this.chooseYearCombobox_SelectedIndexChanged);
            // 
            // chooseSemesterCombobox
            // 
            this.chooseSemesterCombobox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chooseSemesterCombobox.FormattingEnabled = true;
            this.chooseSemesterCombobox.Items.AddRange(new object[] {
            "Tất cả"});
            this.chooseSemesterCombobox.Location = new System.Drawing.Point(138, 94);
            this.chooseSemesterCombobox.Name = "chooseSemesterCombobox";
            this.chooseSemesterCombobox.Size = new System.Drawing.Size(37, 26);
            this.chooseSemesterCombobox.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 18);
            this.label1.TabIndex = 9;
            this.label1.Text = "Chọn học kỳ:";
            // 
            // changePointButton
            // 
            this.changePointButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changePointButton.Location = new System.Drawing.Point(22, 234);
            this.changePointButton.Name = "changePointButton";
            this.changePointButton.Size = new System.Drawing.Size(153, 30);
            this.changePointButton.TabIndex = 8;
            this.changePointButton.Text = "Sửa điểm";
            this.changePointButton.UseVisualStyleBackColor = true;
            this.changePointButton.Click += new System.EventHandler(this.changePointButton_Click);
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backButton.Location = new System.Drawing.Point(22, 270);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(153, 30);
            this.backButton.TabIndex = 7;
            this.backButton.Text = "Thoát";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // okButton
            // 
            this.okButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.okButton.Location = new System.Drawing.Point(22, 198);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(153, 30);
            this.okButton.TabIndex = 6;
            this.okButton.Text = "Xem điểm";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pointDatagrid);
            this.panel1.Location = new System.Drawing.Point(7, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(808, 317);
            this.panel1.TabIndex = 4;
            // 
            // pointDatagrid
            // 
            this.pointDatagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pointDatagrid.Location = new System.Drawing.Point(196, 0);
            this.pointDatagrid.Name = "pointDatagrid";
            this.pointDatagrid.Size = new System.Drawing.Size(609, 317);
            this.pointDatagrid.TabIndex = 0;
            this.pointDatagrid.TabStop = false;
            // 
            // fViewPoint
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 341);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.Name = "fViewPoint";
            this.Text = "Xem điểm";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pointDatagrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox chooseSemesterCombobox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button changePointButton;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView pointDatagrid;
        private System.Windows.Forms.TextBox idStudentTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox chooseYearCombobox;
    }
}