﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StudentManagementApp.DAO;

namespace StudentManagementApp
{
    public partial class fViewInfoAdmin : Form
    {
        public fViewInfoAdmin()
        {
            InitializeComponent();
            LoadInfoStudentList();
        }

        void LoadInfoStudentList()
        {
            studentInfoDatagid.DataSource = InfoStudentDAO.Instance.LoadInfoStudent();
        }

        private void addStudentButton_Click(object sender, EventArgs e)
        {
            fAddStudent form = new fAddStudent();
            this.Hide();
            form.ShowDialog();
            this.Show();
            LoadInfoStudentList();
        }

        private void changeButton_Click(object sender, EventArgs e)
        {
            fChangeInfo form = new fChangeInfo();
            this.Hide();
            form.ShowDialog();
            this.Show();
            LoadInfoStudentList();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            fDeleteInfo form = new fDeleteInfo();
            this.Hide();
            form.ShowDialog();
            this.Show();
            LoadInfoStudentList();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fViewInfoAdmin_Load(object sender, EventArgs e)
        {

        }
    }
}
