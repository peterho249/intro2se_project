﻿using StudentManagementApp.DAO;
using StudentManagementApp.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementApp
{
    public partial class fAddStudent : Form
    {
        public fAddStudent()
        {
            InitializeComponent();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            // Kiem tra ma so sinh vien
            bool IsValidData = true;
            if (studentIdTextbox.Text == "")
            {
                idError.SetError(studentIdTextbox, "Lỗi chưa nhập MSSV");
                IsValidData = false;
            }
            else
            {
                if (InfoStudentDAO.Instance.QueryId(studentIdTextbox.Text) == true)
                {
                    idError.SetError(studentIdTextbox, "ID đã tồn tại");
                    IsValidData = false;
                }
                else
                    idError.Clear();
            }

            // Kiem tra ten
            if (nameTextbox.Text == "")
            {
                nameError.SetError(nameTextbox, "Lỗi chưa nhập họ tên");
                IsValidData = false;
            }
            else
                nameError.Clear();
            // Kiem tra gioi tinh
            if (sexTextBox.Text=="Nữ"||sexTextBox.Text=="Nam")
            {
                sexError.Clear();      
            }
            else
            {
                sexError.SetError(sexTextBox, "Giới tính không hợp lệ");
                IsValidData = false;
            }

            // Kiem tra so dien thoai
            if (phoneNoTextbox.Text != "")
            {
                if (!double.TryParse(phoneNoTextbox.Text, out var n))
                {
                    phonenoError.SetError(phoneNoTextbox, "SDT không hợp lệ");
                    IsValidData = false;
                }
                else
                    phonenoError.Clear();
            }
            else
                phonenoError.Clear();

            // Kiem tra so CMND
            if (idNoTextbox.Text != "")
            {
                if (!double.TryParse(idNoTextbox.Text, out var n))
                {
                    idnoError.SetError(idNoTextbox, "CMND không hợp lệ");
                    IsValidData = false;
                }
                else
                {
                
                    if (InfoStudentDAO.Instance.QueryIdNo(idNoTextbox.Text))
                    {
                        idnoError.SetError(idNoTextbox, "CMND đã tồn tại");
                        IsValidData = false;
                        
                    }
                    else
                        idnoError.Clear();
                }
            }
            else
                idnoError.Clear();

            // Kiem tra email hop le
            if (emailTextbox.Text != "")
            {
                if (!ValidateData.IsValidEmail(emailTextbox.Text))
                {
                    emailError.SetError(emailTextbox, "Email không hợp lệ");
                    IsValidData = false;
                }
                else
                    emailError.Clear();
            }
            else
                emailError.Clear();

            // Kiem tra ngay sinh hop le
            if (!ValidateData.IsValidDate(dobDateTimePicker.Value))
            {
                dobError.SetError(dobDateTimePicker, "Ngày sinh không hợp lệ");
                IsValidData = false;
            }
            else
                dobError.Clear();

            if (!IsValidData)
                return;
            if (InfoStudentDAO.Instance.insertStudent(studentIdTextbox.Text,nameTextbox.Text,sexTextBox.Text, dobDateTimePicker.Value, phoneNoTextbox.Text, idNoTextbox.Text, addressTextbox.Text, emailTextbox.Text))
            {
                AccountDAO.Instance.insertAccount(studentIdTextbox.Text);
                MessageBox.Show("Thêm thành công");
            }
            else
            {
                MessageBox.Show("Lỗi khi thêm");
            }
        }

        private void fAddStudent_Load(object sender, EventArgs e)
        {

        }
    }
}
