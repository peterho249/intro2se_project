﻿using StudentManagementApp.DAO;
using StudentManagementApp.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;

using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace StudentManagementApp
{
    public partial class fChangeInfo : Form
    {
        public fChangeInfo()
        {
            InitializeComponent();
            if (Global.adminFlag)
            {
                studentIdTextbox.Enabled = true;
                nameTextbox.Enabled = false;
                dobDateTimePicker.Enabled = false;
                emailTextbox.Enabled = false;
                phoneNoTextbox.Enabled = false;
                idNoTextbox.Enabled = false;
                addressTextbox.Enabled = false;
                okButton.Enabled = false;
                sexTextBox.Enabled = false;

            }
            else
            {
                studentIdTextbox.Enabled = false;
                nameTextbox.Enabled = true;
                dobDateTimePicker.Enabled = true;
                emailTextbox.Enabled = true;
                phoneNoTextbox.Enabled = true;
                idNoTextbox.Enabled = true;
                addressTextbox.Enabled = true;

                LoadInfoStudent(Global.userName);
            }
        }
        
        void LoadInfoStudent(string idStu)
        {
            InfoStudent info = InfoStudentDAO.Instance.LoadInfoStudent(idStu);

            if (info == null)
            {
                DisableField();
                return;
            }

            studentIdTextbox.Text = info.IdStu;
            nameTextbox.Text = info.Name;
            emailTextbox.Text = info.Email;
            phoneNoTextbox.Text = info.Phone;
            idNoTextbox.Text = info.IdNo;
            addressTextbox.Text = info.Address;
            dobDateTimePicker.Value = info.Birthday;
            sexTextBox.Text = info.Sex;
            okButton.Enabled = true;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            // Kiem tra ma so sinh vien
            bool IsValidData = true;

            // Kiem tra ten
            if (nameTextbox.Text == "")
            {
                nameError.SetError(nameTextbox, "Lỗi chưa nhập họ tên");
                IsValidData = false;
            }
            else
                nameError.Clear();

            // Kiem tra so dien thoai
            if (phoneNoTextbox.Text != "")
            {
                if (!double.TryParse(phoneNoTextbox.Text, out var n))
                {
                    phonenoError.SetError(phoneNoTextbox, "SDT không hợp lệ");
                    IsValidData = false;
                }
                else
                    phonenoError.Clear();
            }
            else
                phonenoError.Clear();

            // Kiem tra so CMND
            if (idNoTextbox.Text != "")
            {
                if (!double.TryParse(idNoTextbox.Text, out var n))
                {
                    idnoError.SetError(idNoTextbox, "CMND không hợp lệ");
                    IsValidData = false;
                }
                else
                {

                    //if (InfoStudentDAO.Instance.QueryIdNo(idNoTextbox.Text))
                    //{
                    //    idnoError.SetError(idNoTextbox, "CMND đã tồn tại");
                    //    IsValidData = false;

                    //}
                    //else
                        idnoError.Clear();
                }
            }
            else
                idnoError.Clear();

            // Kiem tra email hop le
            if (emailTextbox.Text != "")
            {
                if (!ValidateData.IsValidEmail(emailTextbox.Text))
                {
                    emailError.SetError(emailTextbox, "Email không hợp lệ");
                    IsValidData = false;
                }
                else
                    emailError.Clear();
            }
            else
                emailError.Clear();

            // Kiem tra gioi tinh
            if (sexTextBox.Text == "Nữ" || sexTextBox.Text == "Nam")
            {
                sexError.Clear();
            }
            else
            {
                sexError.SetError(sexTextBox, "Giới tính không hợp lệ");
                IsValidData = false;
            }
            // Kiem tra ngay sinh hop le
            if (!ValidateData.IsValidDate(dobDateTimePicker.Value))
            {
                dobError.SetError(dobDateTimePicker, "Ngày sinh không hợp lệ");
                IsValidData = false;
            }
            else
                dobError.Clear();

            if (!IsValidData)
                return;

            if (AccountDAO.Instance.ChangeInfo(studentIdTextbox.Text, nameTextbox.Text,sexTextBox.Text, dobDateTimePicker.Value, phoneNoTextbox.Text, idNoTextbox.Text, addressTextbox.Text, emailTextbox.Text))
            {
                MessageBox.Show("Bạn đã sửa đổi thông tin thành công");
                this.Close();
            }
            else
                MessageBox.Show("Bạn sửa đổi thông tin thất bại");
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void studentIdTextbox_TextChanged(object sender, EventArgs e)
        {
            if (studentIdTextbox.Text.Length == 7)
            {
                nameTextbox.Enabled = true;
                dobDateTimePicker.Enabled = true;
                emailTextbox.Enabled = true;
                phoneNoTextbox.Enabled = true;
                idNoTextbox.Enabled = true;
                addressTextbox.Enabled = true;
                sexTextBox.Enabled = true;
                LoadInfoStudent(studentIdTextbox.Text);
            }
            else
            {
                DisableField();
            }
        }

        private void fChangeInfo_Load(object sender, EventArgs e)
        {

        }

        void DisableField()
        {
            nameTextbox.Enabled = false;
            dobDateTimePicker.Enabled = false;
            emailTextbox.Enabled = false;
            phoneNoTextbox.Enabled = false;
            idNoTextbox.Enabled = false;
            addressTextbox.Enabled = false;
            sexTextBox.Enabled = false;

            nameTextbox.Clear();
            emailTextbox.Clear();
            phoneNoTextbox.Clear();
            idNoTextbox.Clear();
            sexTextBox.Clear();
            addressTextbox.Clear();
            addressTextbox.Enabled = false;

            okButton.Enabled = false;
        }
    }
}
