﻿using StudentManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementApp
{
    public partial class fViewSchedule : Form
    {
        public fViewSchedule()
        {
            InitializeComponent();

            if (Global.adminFlag)
            {
                idStudentTextbox.Enabled = true;
                okButton.Enabled = false;
                changeScheduleButton.Enabled = true;
            }
            else
            {
                idStudentTextbox.Enabled = false;
                changeScheduleButton.Enabled = false;
                idStudentTextbox.Text = Global.userName;
            }
        }
        
        public void IniChooseYearCombobox(string idStu)
        {
            chooseSemesterCombobox.DataSource = null;
            chooseYearCombobox.DataSource = null;
            // Load dữ liệu cho ComboBox Year
            chooseYearCombobox.Items.Clear();
            List<string> yearList= DAO.ScheduleDAO.Instance.ListYear();
            chooseYearCombobox.DataSource = yearList;

            // Load dữ liệu cho ComboBox Semester
            yearList = DAO.ScheduleDAO.Instance.ListSemesterForStudent(idStu, chooseYearCombobox.SelectedItem.ToString());
            if (yearList.Count == 0)
                yearList.Add("1");
            chooseSemesterCombobox.DataSource = yearList;
        }

        public void UpdateDatagrid()
        {
            int semester = Int32.Parse(chooseSemesterCombobox.SelectedItem.ToString());
            string year = chooseYearCombobox.SelectedItem.ToString();

            scheduleDatagrid.DataSource = DAO.ScheduleDAO.Instance.ListSchedule(idStudentTextbox.Text, semester, year);
            scheduleDatagrid.Enabled = false;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            UpdateDatagrid();
        }

        private void idStudentTextbox_TextChanged(object sender, EventArgs e)
        {
            if (idStudentTextbox.Text.Length == 7 && DAO.InfoStudentDAO.Instance.QueryId(idStudentTextbox.Text))
            {
                // Load sữ liệu lên cho hai combo box Học kỳ và Năm
                IniChooseYearCombobox(idStudentTextbox.Text.ToString());
                UpdateDatagrid();

                okButton.Enabled = true;
            }
            else
                okButton.Enabled = false;
        }

        private bool CheckSubject(string IdSub)
        {
            List<string> IdSubList = DAO.ScheduleDAO.Instance.ListIdSub();
            foreach (string dr in IdSubList)
            {
                if (dr == IdSub)
                    return true;
            }
            return false;
        }

        private void fViewSchedule_Load(object sender, EventArgs e)
        {

        }
        
        private void backButton_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void changeScheduleButton_Click(object sender, EventArgs e)
        {
            if (scheduleDatagrid.Enabled == false)
            {
                scheduleDatagrid.Enabled = true;
            }
            else
            {
                DataTable Schedule = new DataTable();

                // Chuyển thành datatable
                Schedule = ((DataTable)scheduleDatagrid.DataSource).Copy();
                foreach (DataGridViewColumn column in scheduleDatagrid.Columns)
                {
                    if (!column.Visible)
                    {
                        Schedule.Columns.Remove(column.Name);
                    }
                }

                // Kiểm tra các môn học có thỏa điều kiện hay không
                foreach (DataRow dr in Schedule.Rows)
                {
                    if (CheckSubject((string)dr[0]) == false)
                    {
                        MessageBox.Show("MÃ MÔN HỌC CỦA BẠN KHÔNG CHÍNH XÁC");
                        return;
                    }
                }

                // Tiến hành Update thời khóa biểu
                int semester = Int32.Parse(chooseSemesterCombobox.SelectedItem.ToString());
                string year = chooseYearCombobox.SelectedItem.ToString();

                // Xóa toàn bọ thời khóa biểu
                DAO.ScheduleDAO.Instance.DeleteAll(idStudentTextbox.Text);

                // Đổ dữ liệu lại
                foreach (DataRow dr in Schedule.Rows)
                {
                    
                    DAO.ScheduleDAO.Instance.InsertItemSchedule(idStudentTextbox.Text, (string)dr[0], semester, year, (string)dr[2]);
                }

                MessageBox.Show("BẠN ĐÃ THAY ĐỔI THỜI KHÓA BIẾU THÀNH CÔNG");
            }
        }

		private void GlobalChangeButton_Click(object sender, EventArgs e)
		{

        }
    }
}
