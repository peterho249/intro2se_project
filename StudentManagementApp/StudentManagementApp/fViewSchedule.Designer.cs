﻿namespace StudentManagementApp
{
    partial class fViewSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.changeScheduleButton = new System.Windows.Forms.Button();
            this.idStudentTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chooseYearCombobox = new System.Windows.Forms.ComboBox();
            this.chooseSemesterCombobox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.backButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.scheduleDatagrid = new System.Windows.Forms.DataGridView();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scheduleDatagrid)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.changeScheduleButton);
            this.panel2.Controls.Add(this.idStudentTextbox);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.chooseYearCombobox);
            this.panel2.Controls.Add(this.chooseSemesterCombobox);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.backButton);
            this.panel2.Controls.Add(this.okButton);
            this.panel2.Location = new System.Drawing.Point(7, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(194, 317);
            this.panel2.TabIndex = 3;
            // 
            // changeScheduleButton
            // 
            this.changeScheduleButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeScheduleButton.Location = new System.Drawing.Point(22, 160);
            this.changeScheduleButton.Name = "changeScheduleButton";
            this.changeScheduleButton.Size = new System.Drawing.Size(153, 30);
            this.changeScheduleButton.TabIndex = 4;
            this.changeScheduleButton.Text = "Sửa thời khóa biểu";
            this.changeScheduleButton.UseVisualStyleBackColor = true;
            this.changeScheduleButton.Click += new System.EventHandler(this.changeScheduleButton_Click);
            // 
            // idStudentTextbox
            // 
            this.idStudentTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idStudentTextbox.Location = new System.Drawing.Point(25, 38);
            this.idStudentTextbox.Name = "idStudentTextbox";
            this.idStudentTextbox.Size = new System.Drawing.Size(136, 26);
            this.idStudentTextbox.TabIndex = 1;
            this.idStudentTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.idStudentTextbox.TextChanged += new System.EventHandler(this.idStudentTextbox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(38, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 18);
            this.label2.TabIndex = 17;
            this.label2.Text = "Chọn sinh viên";
            // 
            // chooseYearCombobox
            // 
            this.chooseYearCombobox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chooseYearCombobox.FormattingEnabled = true;
            this.chooseYearCombobox.Items.AddRange(new object[] {
            "Tất cả"});
            this.chooseYearCombobox.Location = new System.Drawing.Point(8, 98);
            this.chooseYearCombobox.Name = "chooseYearCombobox";
            this.chooseYearCombobox.Size = new System.Drawing.Size(127, 26);
            this.chooseYearCombobox.TabIndex = 2;
            // 
            // chooseSemesterCombobox
            // 
            this.chooseSemesterCombobox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chooseSemesterCombobox.FormattingEnabled = true;
            this.chooseSemesterCombobox.Items.AddRange(new object[] {
            "Tất cả"});
            this.chooseSemesterCombobox.Location = new System.Drawing.Point(141, 98);
            this.chooseSemesterCombobox.Name = "chooseSemesterCombobox";
            this.chooseSemesterCombobox.Size = new System.Drawing.Size(45, 26);
            this.chooseSemesterCombobox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 18);
            this.label1.TabIndex = 14;
            this.label1.Text = "Chọn học kỳ:";
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backButton.Location = new System.Drawing.Point(22, 257);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(153, 30);
            this.backButton.TabIndex = 7;
            this.backButton.Text = "Thoát";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click_1);
            // 
            // okButton
            // 
            this.okButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.okButton.Location = new System.Drawing.Point(22, 212);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(153, 30);
            this.okButton.TabIndex = 6;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.scheduleDatagrid);
            this.panel1.Location = new System.Drawing.Point(207, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(612, 317);
            this.panel1.TabIndex = 2;
            // 
            // scheduleDatagrid
            // 
            this.scheduleDatagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.scheduleDatagrid.Location = new System.Drawing.Point(0, 0);
            this.scheduleDatagrid.Name = "scheduleDatagrid";
            this.scheduleDatagrid.Size = new System.Drawing.Size(609, 317);
            this.scheduleDatagrid.TabIndex = 0;
            this.scheduleDatagrid.TabStop = false;
            // 
            // fViewSchedule
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 341);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.Name = "fViewSchedule";
            this.Text = "Xem thời khóa biểu";
            this.Load += new System.EventHandler(this.fViewSchedule_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scheduleDatagrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button changeScheduleButton;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView scheduleDatagrid;
        private System.Windows.Forms.TextBox idStudentTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox chooseYearCombobox;
        private System.Windows.Forms.ComboBox chooseSemesterCombobox;
        private System.Windows.Forms.Label label1;
	}
}