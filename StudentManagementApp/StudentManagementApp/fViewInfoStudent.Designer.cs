﻿namespace StudentManagementApp
{
    partial class fViewInfoStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.sexTextBox = new System.Windows.Forms.TextBox();
            this.addressTextbox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.idNoTextbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.phoneNoTextbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.emailTextbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dobTextbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.nameTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.studentIdTextbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.backButton = new System.Windows.Forms.Button();
            this.changeInfoButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.sexTextBox);
            this.panel1.Controls.Add(this.addressTextbox);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.idNoTextbox);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.phoneNoTextbox);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.emailTextbox);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.dobTextbox);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.nameTextbox);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.studentIdTextbox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(818, 243);
            this.panel1.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 12F);
            this.label8.Location = new System.Drawing.Point(16, 93);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 18);
            this.label8.TabIndex = 15;
            this.label8.Text = "Giới tính:";
            // 
            // sexTextBox
            // 
            this.sexTextBox.Font = new System.Drawing.Font("Arial", 12F);
            this.sexTextBox.Location = new System.Drawing.Point(136, 87);
            this.sexTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.sexTextBox.Multiline = true;
            this.sexTextBox.Name = "sexTextBox";
            this.sexTextBox.ReadOnly = true;
            this.sexTextBox.Size = new System.Drawing.Size(262, 25);
            this.sexTextBox.TabIndex = 14;
            this.sexTextBox.TabStop = false;
            // 
            // addressTextbox
            // 
            this.addressTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addressTextbox.Location = new System.Drawing.Point(539, 103);
            this.addressTextbox.Multiline = true;
            this.addressTextbox.Name = "addressTextbox";
            this.addressTextbox.ReadOnly = true;
            this.addressTextbox.Size = new System.Drawing.Size(262, 85);
            this.addressTextbox.TabIndex = 13;
            this.addressTextbox.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(435, 109);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 18);
            this.label7.TabIndex = 12;
            this.label7.Text = "Địa chỉ:";
            // 
            // idNoTextbox
            // 
            this.idNoTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idNoTextbox.Location = new System.Drawing.Point(539, 58);
            this.idNoTextbox.Name = "idNoTextbox";
            this.idNoTextbox.ReadOnly = true;
            this.idNoTextbox.Size = new System.Drawing.Size(262, 26);
            this.idNoTextbox.TabIndex = 11;
            this.idNoTextbox.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(435, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Số CMND:";
            // 
            // phoneNoTextbox
            // 
            this.phoneNoTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phoneNoTextbox.Location = new System.Drawing.Point(539, 14);
            this.phoneNoTextbox.Name = "phoneNoTextbox";
            this.phoneNoTextbox.ReadOnly = true;
            this.phoneNoTextbox.Size = new System.Drawing.Size(262, 26);
            this.phoneNoTextbox.TabIndex = 9;
            this.phoneNoTextbox.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(435, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Số điện thoại:";
            // 
            // emailTextbox
            // 
            this.emailTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailTextbox.Location = new System.Drawing.Point(136, 163);
            this.emailTextbox.Name = "emailTextbox";
            this.emailTextbox.ReadOnly = true;
            this.emailTextbox.Size = new System.Drawing.Size(262, 26);
            this.emailTextbox.TabIndex = 7;
            this.emailTextbox.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Email:";
            // 
            // dobTextbox
            // 
            this.dobTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dobTextbox.Location = new System.Drawing.Point(136, 125);
            this.dobTextbox.Name = "dobTextbox";
            this.dobTextbox.ReadOnly = true;
            this.dobTextbox.Size = new System.Drawing.Size(262, 26);
            this.dobTextbox.TabIndex = 5;
            this.dobTextbox.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ngày sinh:";
            // 
            // nameTextbox
            // 
            this.nameTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameTextbox.Location = new System.Drawing.Point(136, 50);
            this.nameTextbox.Name = "nameTextbox";
            this.nameTextbox.ReadOnly = true;
            this.nameTextbox.Size = new System.Drawing.Size(262, 26);
            this.nameTextbox.TabIndex = 3;
            this.nameTextbox.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Họ tên:";
            // 
            // studentIdTextbox
            // 
            this.studentIdTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentIdTextbox.Location = new System.Drawing.Point(136, 14);
            this.studentIdTextbox.Name = "studentIdTextbox";
            this.studentIdTextbox.ReadOnly = true;
            this.studentIdTextbox.Size = new System.Drawing.Size(262, 26);
            this.studentIdTextbox.TabIndex = 1;
            this.studentIdTextbox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "MSSV:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.backButton);
            this.panel2.Controls.Add(this.changeInfoButton);
            this.panel2.Controls.Add(this.okButton);
            this.panel2.Location = new System.Drawing.Point(4, 261);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(817, 68);
            this.panel2.TabIndex = 1;
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backButton.Location = new System.Drawing.Point(556, 21);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(153, 30);
            this.backButton.TabIndex = 3;
            this.backButton.Text = "Thoát";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // changeInfoButton
            // 
            this.changeInfoButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeInfoButton.Location = new System.Drawing.Point(328, 21);
            this.changeInfoButton.Name = "changeInfoButton";
            this.changeInfoButton.Size = new System.Drawing.Size(153, 30);
            this.changeInfoButton.TabIndex = 2;
            this.changeInfoButton.Text = "Sửa thông tin";
            this.changeInfoButton.UseVisualStyleBackColor = true;
            this.changeInfoButton.Click += new System.EventHandler(this.changeInfoButton_Click);
            // 
            // okButton
            // 
            this.okButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.okButton.Location = new System.Drawing.Point(111, 21);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(153, 30);
            this.okButton.TabIndex = 1;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // fViewInfoStudent
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 341);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "fViewInfoStudent";
            this.Text = "Xem Thông Tin Sinh Viên";
            this.Load += new System.EventHandler(this.fViewInfoStudent_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox addressTextbox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox idNoTextbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox phoneNoTextbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox emailTextbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox dobTextbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox nameTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox studentIdTextbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button changeInfoButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.TextBox sexTextBox;
        private System.Windows.Forms.Label label8;
    }
}