﻿using StudentManagementApp.DAO;
using StudentManagementApp.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementApp
{
    public partial class fChangePassword : Form
    {
        public fChangePassword()
        {
            InitializeComponent();
            usernameTextbox.Text = Global.userName;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (currentPasswordTextbox.TextLength == 0 || newPasswordTextbox.TextLength == 0 ||
                reenterPasswordTextbox.TextLength == 0)
                MessageBox.Show("Các ô trống phải được điền đầy đủ!", "Thông báo");
            else
            {
                if (currentPasswordTextbox.Text == newPasswordTextbox.Text)
                    MessageBox.Show("Mật khẩu mới phải khác mật khẩu cũ!", "Thông báo");
                else
                {
                    if (newPasswordTextbox.Text != reenterPasswordTextbox.Text)
                        MessageBox.Show("Mật khẩu xác nhận không khớp với mật khẩu mới!", "Thông báo");
                    else
                    {
                        if (AccountDAO.Instance.ChangePassword(usernameTextbox.Text, currentPasswordTextbox.Text, newPasswordTextbox.Text))
                        {
                            MessageBox.Show("Đổi mật khẩu thành công!", "Thông báo");
                            this.Close();
                        }
                        else
                            MessageBox.Show("Đổi mật khẩu thất bại!", "Thông báo");
                    }
                }
            }
            currentPasswordTextbox.Text = "";
            newPasswordTextbox.Text = "";
            reenterPasswordTextbox.Text = "";
        }

        private void fChangePassword_Load(object sender, EventArgs e)
        {

        }
    }
}
