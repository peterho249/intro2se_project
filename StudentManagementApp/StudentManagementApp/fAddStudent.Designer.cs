﻿namespace StudentManagementApp
{
    partial class fAddStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel2 = new System.Windows.Forms.Panel();
            this.backButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.sexTextBox = new System.Windows.Forms.TextBox();
            this.dobDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.addressTextbox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.idNoTextbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.phoneNoTextbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.emailTextbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nameTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.studentIdTextbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.idError = new System.Windows.Forms.ErrorProvider(this.components);
            this.nameError = new System.Windows.Forms.ErrorProvider(this.components);
            this.idnoError = new System.Windows.Forms.ErrorProvider(this.components);
            this.emailError = new System.Windows.Forms.ErrorProvider(this.components);
            this.phonenoError = new System.Windows.Forms.ErrorProvider(this.components);
            this.dobError = new System.Windows.Forms.ErrorProvider(this.components);
            this.sexError = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.idError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nameError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.idnoError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phonenoError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dobError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sexError)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.backButton);
            this.panel2.Controls.Add(this.okButton);
            this.panel2.Location = new System.Drawing.Point(4, 264);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(817, 68);
            this.panel2.TabIndex = 3;
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backButton.Location = new System.Drawing.Point(438, 21);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(153, 30);
            this.backButton.TabIndex = 10;
            this.backButton.Text = "Thoát";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // okButton
            // 
            this.okButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.okButton.Location = new System.Drawing.Point(255, 21);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(153, 30);
            this.okButton.TabIndex = 9;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.sexTextBox);
            this.panel1.Controls.Add(this.dobDateTimePicker);
            this.panel1.Controls.Add(this.addressTextbox);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.idNoTextbox);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.phoneNoTextbox);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.emailTextbox);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.nameTextbox);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.studentIdTextbox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(4, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(818, 243);
            this.panel1.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 12F);
            this.label8.Location = new System.Drawing.Point(14, 102);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 18);
            this.label8.TabIndex = 13;
            this.label8.Text = "Giới tính:";
            // 
            // sexTextBox
            // 
            this.sexTextBox.Font = new System.Drawing.Font("Arial", 12F);
            this.sexTextBox.Location = new System.Drawing.Point(136, 96);
            this.sexTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.sexTextBox.Multiline = true;
            this.sexTextBox.Name = "sexTextBox";
            this.sexTextBox.Size = new System.Drawing.Size(262, 25);
            this.sexTextBox.TabIndex = 3;
            // 
            // dobDateTimePicker
            // 
            this.dobDateTimePicker.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dobDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dobDateTimePicker.Location = new System.Drawing.Point(136, 139);
            this.dobDateTimePicker.Margin = new System.Windows.Forms.Padding(2);
            this.dobDateTimePicker.Name = "dobDateTimePicker";
            this.dobDateTimePicker.Size = new System.Drawing.Size(262, 26);
            this.dobDateTimePicker.TabIndex = 4;
            // 
            // addressTextbox
            // 
            this.addressTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addressTextbox.Location = new System.Drawing.Point(539, 99);
            this.addressTextbox.Multiline = true;
            this.addressTextbox.Name = "addressTextbox";
            this.addressTextbox.Size = new System.Drawing.Size(262, 106);
            this.addressTextbox.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(435, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 18);
            this.label7.TabIndex = 12;
            this.label7.Text = "Địa chỉ:";
            // 
            // idNoTextbox
            // 
            this.idNoTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idNoTextbox.Location = new System.Drawing.Point(539, 55);
            this.idNoTextbox.Name = "idNoTextbox";
            this.idNoTextbox.Size = new System.Drawing.Size(262, 26);
            this.idNoTextbox.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(435, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Số CMND:";
            // 
            // phoneNoTextbox
            // 
            this.phoneNoTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phoneNoTextbox.Location = new System.Drawing.Point(539, 14);
            this.phoneNoTextbox.Name = "phoneNoTextbox";
            this.phoneNoTextbox.Size = new System.Drawing.Size(262, 26);
            this.phoneNoTextbox.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(435, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Số điện thoại:";
            // 
            // emailTextbox
            // 
            this.emailTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailTextbox.Location = new System.Drawing.Point(136, 180);
            this.emailTextbox.Name = "emailTextbox";
            this.emailTextbox.Size = new System.Drawing.Size(262, 26);
            this.emailTextbox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(14, 182);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Email:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ngày sinh: *";
            // 
            // nameTextbox
            // 
            this.nameTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameTextbox.Location = new System.Drawing.Point(136, 55);
            this.nameTextbox.Name = "nameTextbox";
            this.nameTextbox.Size = new System.Drawing.Size(262, 26);
            this.nameTextbox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Họ tên: *";
            // 
            // studentIdTextbox
            // 
            this.studentIdTextbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentIdTextbox.Location = new System.Drawing.Point(136, 14);
            this.studentIdTextbox.Name = "studentIdTextbox";
            this.studentIdTextbox.Size = new System.Drawing.Size(262, 26);
            this.studentIdTextbox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "MSSV: *";
            // 
            // idError
            // 
            this.idError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.idError.ContainerControl = this;
            // 
            // nameError
            // 
            this.nameError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.nameError.ContainerControl = this;
            // 
            // idnoError
            // 
            this.idnoError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.idnoError.ContainerControl = this;
            // 
            // emailError
            // 
            this.emailError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.emailError.ContainerControl = this;
            // 
            // phonenoError
            // 
            this.phonenoError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.phonenoError.ContainerControl = this;
            // 
            // dobError
            // 
            this.dobError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.dobError.ContainerControl = this;
            // 
            // sexError
            // 
            this.sexError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.sexError.ContainerControl = this;
            // 
            // fAddStudent
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 341);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.Name = "fAddStudent";
            this.Text = "Thêm thông tin sinh viên";
            this.Load += new System.EventHandler(this.fAddStudent_Load);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.idError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nameError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.idnoError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phonenoError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dobError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sexError)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox addressTextbox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox idNoTextbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox phoneNoTextbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox emailTextbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox nameTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox studentIdTextbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ErrorProvider idError;
        private System.Windows.Forms.ErrorProvider nameError;
        private System.Windows.Forms.ErrorProvider idnoError;
        private System.Windows.Forms.ErrorProvider emailError;
        private System.Windows.Forms.ErrorProvider phonenoError;
        private System.Windows.Forms.DateTimePicker dobDateTimePicker;
        private System.Windows.Forms.ErrorProvider dobError;
        private System.Windows.Forms.TextBox sexTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ErrorProvider sexError;
    }
}