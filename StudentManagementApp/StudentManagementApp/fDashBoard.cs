﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementApp
{
    public partial class fDashBoard : Form
    {
        public fDashBoard()
        {
            InitializeComponent();
            if (Global.adminFlag)
            {
                addInfoButton.Enabled = true;
                deleteInfoButton.Enabled = true;
                printPointButton.Enabled = true;
                viewSubjectButton.Enabled = true;
                modScoreTableButton.Enabled = true;
                addScheduleButton.Enabled = true;
            }
            else
            {
                addInfoButton.Enabled = false;
                deleteInfoButton.Enabled = false;
                printPointButton.Enabled = false;
                viewSubjectButton.Enabled = false;
                modScoreTableButton.Enabled = false;
                addScheduleButton.Enabled = false;
            }

            currentUserLabel.Text = Global.userName;
        }

        private void viewInfoButton_Click(object sender, EventArgs e)
        {
            if (Global.adminFlag)
            {
                fViewInfoAdmin form = new fViewInfoAdmin();
                form.ShowDialog();
            }
            else
            {
                fViewInfoStudent form = new fViewInfoStudent();
                form.ShowDialog();
            }
        }

        private void changeInfoButton_Click(object sender, EventArgs e)
        {
            fChangeInfo form = new fChangeInfo();
            form.ShowDialog();
        }

        private void deleteInfoButton_Click(object sender, EventArgs e)
        {
            fDeleteInfo form = new fDeleteInfo();
            form.ShowDialog();
        }

        private void viewScheduleButton_Click(object sender, EventArgs e)
        {
            fViewSchedule form = new fViewSchedule();
            form.ShowDialog();
        }

        private void viewPointButton_Click(object sender, EventArgs e)
        {
            fViewPoint form = new fViewPoint();
            form.ShowDialog();
        }

        private void changePasswordButton_Click(object sender, EventArgs e)
        {
            fChangePassword form = new fChangePassword();
            form.ShowDialog();
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn đăng xuất không?", "Thông báo" ,MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                this.Close();
            }
        }

        private void addInfoButton_Click(object sender, EventArgs e)
        {
            fAddStudent form = new fAddStudent();
            form.ShowDialog();
        }

        private void printPointButton_Click(object sender, EventArgs e)
        {
            fPrintPointTable form = new fPrintPointTable();
            form.ShowDialog();
        }

        private void viewSubjectButton_Click(object sender, EventArgs e)
        {
            fViewSubject form = new fViewSubject();
            form.ShowDialog();
        }

        private void addScheduleButton_Click(object sender, EventArgs e)
        {
            fAddSchedule form = new fAddSchedule();
            form.ShowDialog();
        }

        private void modScoreTableButton_Click(object sender, EventArgs e)
        {
            fModifyScore form = new fModifyScore();
            form.ShowDialog();
        }
    }
}