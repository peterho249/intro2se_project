﻿using StudentManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementApp
{
    public partial class fViewPoint : Form
    {
        public static string curIdStu = "";
        public static string curYear = "";
        public static string curSemester = "";
        public fViewPoint()
        {
            InitializeComponent();
            this.AcceptButton = okButton;
            if (Global.adminFlag)
            {
                idStudentTextbox.Enabled = true;
                okButton.Enabled = false;
            }
            else
            {
                okButton.Enabled = true;
                idStudentTextbox.Enabled = false;
                idStudentTextbox.Text = Global.userName;
                loadYearList();
                updateGrid();
            }
            changePointButton.Enabled = false;
        }

        bool IdValidation(string userName)
        {
            return AccountDAO.Instance.IdValidation(userName);
        }

        private void idStudentTextbox_TextChanged(object sender, EventArgs e)
        {
            if (idStudentTextbox.Text.Length == 7)
            {
                string mssv = idStudentTextbox.Text;
                if (IdValidation(mssv))
                {
                    loadYearList();
                    okButton.Enabled = true;
                    updateGrid();
                    if (Global.adminFlag)
                    {
                        changePointButton.Enabled = true;
                    }
                }
                else
                {
                    MessageBox.Show("Mã số sinh viên không tồn tại", "Thông báo");
                    okButton.Enabled = false;
                    changePointButton.Enabled = false;
                }
            }
            else
            {
                okButton.Enabled = false;
                changePointButton.Enabled = false;
            }
        }

        private void chooseYearCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;

            if (comboBox.SelectedItem == null) return;

            string selected = comboBox.SelectedItem as string;

            loadSemesterListByYear(selected);
        }

        private void loadYearList()
        {
            List<string> yearList = ScoreStudentDAO.Instance.GetListYear(idStudentTextbox.Text);
            if (yearList.Count == 0)
            {
                yearList.Add(DateTime.Now.Year + "-" + (DateTime.Now.Year + 1));
            }
            chooseYearCombobox.DataSource = yearList;
        }

        private void loadSemesterListByYear(string year)
        {
            List<string> semesterList = ScoreStudentDAO.Instance.GetListSemester(year);
            if (semesterList.Count == 0)
                semesterList.Add("1");
            chooseSemesterCombobox.DataSource = semesterList;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            updateGrid();
        }

        public void updateGrid()
        {
            int semester = Int32.Parse(chooseSemesterCombobox.SelectedItem.ToString());
            string year = chooseYearCombobox.SelectedItem.ToString();

            pointDatagrid.DataSource = ScoreStudentDAO.Instance.LoadScoreTable(idStudentTextbox.Text, semester, year);
        }

        private void changePointButton_Click(object sender, EventArgs e)
        {
            updateStaticInfo();
            fChangeScore form = new fChangeScore();
            form.ShowDialog();
            updateGrid();
        }

        private void updateStaticInfo()
        {
            curIdStu = this.idStudentTextbox.Text;
            curYear = this.chooseYearCombobox.SelectedValue.ToString();
            curSemester = this.chooseSemesterCombobox.SelectedValue.ToString();
        }
    }
}
