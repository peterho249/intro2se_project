﻿namespace StudentManagementApp
{
    partial class fModifyScore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listStuGrid = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.subjectCombobox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chooseYearCombobox = new System.Windows.Forms.ComboBox();
            this.chooseSemesterCombobox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.listStuGrid)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // listStuGrid
            // 
            this.listStuGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listStuGrid.Location = new System.Drawing.Point(210, 12);
            this.listStuGrid.Name = "listStuGrid";
            this.listStuGrid.Size = new System.Drawing.Size(609, 317);
            this.listStuGrid.TabIndex = 9;
            this.listStuGrid.TabStop = false;
            this.listStuGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.listStuGrid_CellValueChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.subjectCombobox);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.chooseYearCombobox);
            this.panel2.Controls.Add(this.chooseSemesterCombobox);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.okButton);
            this.panel2.Controls.Add(this.backButton);
            this.panel2.Location = new System.Drawing.Point(10, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(194, 317);
            this.panel2.TabIndex = 8;
            // 
            // subjectCombobox
            // 
            this.subjectCombobox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectCombobox.FormattingEnabled = true;
            this.subjectCombobox.Location = new System.Drawing.Point(22, 94);
            this.subjectCombobox.Name = "subjectCombobox";
            this.subjectCombobox.Size = new System.Drawing.Size(153, 26);
            this.subjectCombobox.TabIndex = 1;
            this.subjectCombobox.SelectedIndexChanged += new System.EventHandler(this.subjectCombobox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(43, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 18);
            this.label2.TabIndex = 12;
            this.label2.Text = "Chọn môn học:";
            // 
            // chooseYearCombobox
            // 
            this.chooseYearCombobox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chooseYearCombobox.FormattingEnabled = true;
            this.chooseYearCombobox.Location = new System.Drawing.Point(22, 30);
            this.chooseYearCombobox.Name = "chooseYearCombobox";
            this.chooseYearCombobox.Size = new System.Drawing.Size(110, 26);
            this.chooseYearCombobox.TabIndex = 2;
            this.chooseYearCombobox.SelectedIndexChanged += new System.EventHandler(this.chooseYearCombobox_SelectedIndexChanged);
            // 
            // chooseSemesterCombobox
            // 
            this.chooseSemesterCombobox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chooseSemesterCombobox.FormattingEnabled = true;
            this.chooseSemesterCombobox.Location = new System.Drawing.Point(138, 30);
            this.chooseSemesterCombobox.Name = "chooseSemesterCombobox";
            this.chooseSemesterCombobox.Size = new System.Drawing.Size(37, 26);
            this.chooseSemesterCombobox.TabIndex = 3;
            this.chooseSemesterCombobox.SelectedIndexChanged += new System.EventHandler(this.chooseSemesterCombobox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 18);
            this.label1.TabIndex = 9;
            this.label1.Text = "Chọn học kỳ:";
            // 
            // okButton
            // 
            this.okButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.okButton.Location = new System.Drawing.Point(22, 234);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(153, 30);
            this.okButton.TabIndex = 4;
            this.okButton.Text = "Lưu";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backButton.Location = new System.Drawing.Point(22, 270);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(153, 30);
            this.backButton.TabIndex = 6;
            this.backButton.Text = "Thoát";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // fModifyScore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 341);
            this.Controls.Add(this.listStuGrid);
            this.Controls.Add(this.panel2);
            this.Name = "fModifyScore";
            this.Text = "Chỉnh sửa điểm";
            ((System.ComponentModel.ISupportInitialize)(this.listStuGrid)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView listStuGrid;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox subjectCombobox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox chooseYearCombobox;
        private System.Windows.Forms.ComboBox chooseSemesterCombobox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button backButton;
    }
}