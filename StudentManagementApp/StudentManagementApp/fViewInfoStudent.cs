﻿using StudentManagementApp.DAO;
using StudentManagementApp.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementApp
{
    public partial class fViewInfoStudent : Form
    {
        public fViewInfoStudent()
        {
            InitializeComponent();
            LoadInfoStudent(Global.userName);
        }

        void LoadInfoStudent(string idStu)
        {
            InfoStudent info = InfoStudentDAO.Instance.LoadInfoStudent(idStu);

            studentIdTextbox.Text = info.IdStu;
            nameTextbox.Text = info.Name;
            emailTextbox.Text = info.Email;
            phoneNoTextbox.Text = info.Phone;
            idNoTextbox.Text = info.IdNo;
            sexTextBox.Text = info.Sex;
            addressTextbox.Text = info.Address;
            dobTextbox.Text = Convert.ToDateTime(info.Birthday).ToString("dd/MM/yyyy");
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void changeInfoButton_Click(object sender, EventArgs e)
        {
            fChangeInfo form = new fChangeInfo();
            this.Hide();
            form.ShowDialog();
            this.Show();
            LoadInfoStudent(Global.userName);
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fViewInfoStudent_Load(object sender, EventArgs e)
        {

        }
    }
}
