﻿namespace StudentManagementApp
{
    partial class fDashBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fDashBoard));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.addScheduleButton = new System.Windows.Forms.Button();
            this.modScoreTableButton = new System.Windows.Forms.Button();
            this.changePasswordButton = new System.Windows.Forms.Button();
            this.logoutButton = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.viewSubjectButton = new System.Windows.Forms.Button();
            this.printPointButton = new System.Windows.Forms.Button();
            this.viewScheduleButton = new System.Windows.Forms.Button();
            this.viewPointButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.addInfoButton = new System.Windows.Forms.Button();
            this.viewInfoButton = new System.Windows.Forms.Button();
            this.changeInfoButton = new System.Windows.Forms.Button();
            this.deleteInfoButton = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.currentUserLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(7, 38);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(547, 385);
            this.panel1.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.addScheduleButton);
            this.panel4.Controls.Add(this.modScoreTableButton);
            this.panel4.Controls.Add(this.changePasswordButton);
            this.panel4.Controls.Add(this.logoutButton);
            this.panel4.Location = new System.Drawing.Point(375, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(158, 375);
            this.panel4.TabIndex = 8;
            // 
            // addScheduleButton
            // 
            this.addScheduleButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addScheduleButton.Image = ((System.Drawing.Image)(resources.GetObject("addScheduleButton.Image")));
            this.addScheduleButton.Location = new System.Drawing.Point(16, 15);
            this.addScheduleButton.Name = "addScheduleButton";
            this.addScheduleButton.Size = new System.Drawing.Size(128, 84);
            this.addScheduleButton.TabIndex = 9;
            this.addScheduleButton.Text = "Thêm thời khóa biểu";
            this.addScheduleButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.addScheduleButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.addScheduleButton.UseVisualStyleBackColor = true;
            this.addScheduleButton.Click += new System.EventHandler(this.addScheduleButton_Click);
            // 
            // modScoreTableButton
            // 
            this.modScoreTableButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modScoreTableButton.Image = ((System.Drawing.Image)(resources.GetObject("modScoreTableButton.Image")));
            this.modScoreTableButton.Location = new System.Drawing.Point(16, 105);
            this.modScoreTableButton.Name = "modScoreTableButton";
            this.modScoreTableButton.Size = new System.Drawing.Size(128, 84);
            this.modScoreTableButton.TabIndex = 10;
            this.modScoreTableButton.Text = "Sửa bảng điểm";
            this.modScoreTableButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.modScoreTableButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.modScoreTableButton.UseVisualStyleBackColor = true;
            this.modScoreTableButton.Click += new System.EventHandler(this.modScoreTableButton_Click);
            // 
            // changePasswordButton
            // 
            this.changePasswordButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changePasswordButton.Image = ((System.Drawing.Image)(resources.GetObject("changePasswordButton.Image")));
            this.changePasswordButton.Location = new System.Drawing.Point(16, 195);
            this.changePasswordButton.Name = "changePasswordButton";
            this.changePasswordButton.Size = new System.Drawing.Size(128, 84);
            this.changePasswordButton.TabIndex = 11;
            this.changePasswordButton.Text = "Đổi mật khẩu";
            this.changePasswordButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.changePasswordButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.changePasswordButton.UseVisualStyleBackColor = true;
            this.changePasswordButton.Click += new System.EventHandler(this.changePasswordButton_Click);
            // 
            // logoutButton
            // 
            this.logoutButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logoutButton.Image = ((System.Drawing.Image)(resources.GetObject("logoutButton.Image")));
            this.logoutButton.Location = new System.Drawing.Point(16, 285);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(128, 84);
            this.logoutButton.TabIndex = 12;
            this.logoutButton.Text = "Đăng xuất";
            this.logoutButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.logoutButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.viewSubjectButton);
            this.panel3.Controls.Add(this.printPointButton);
            this.panel3.Controls.Add(this.viewScheduleButton);
            this.panel3.Controls.Add(this.viewPointButton);
            this.panel3.Location = new System.Drawing.Point(196, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(157, 375);
            this.panel3.TabIndex = 7;
            // 
            // viewSubjectButton
            // 
            this.viewSubjectButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewSubjectButton.Image = ((System.Drawing.Image)(resources.GetObject("viewSubjectButton.Image")));
            this.viewSubjectButton.Location = new System.Drawing.Point(15, 195);
            this.viewSubjectButton.Name = "viewSubjectButton";
            this.viewSubjectButton.Size = new System.Drawing.Size(128, 84);
            this.viewSubjectButton.TabIndex = 7;
            this.viewSubjectButton.Text = "Xem danh sách môn học";
            this.viewSubjectButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.viewSubjectButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.viewSubjectButton.UseVisualStyleBackColor = true;
            this.viewSubjectButton.Click += new System.EventHandler(this.viewSubjectButton_Click);
            // 
            // printPointButton
            // 
            this.printPointButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.printPointButton.Image = ((System.Drawing.Image)(resources.GetObject("printPointButton.Image")));
            this.printPointButton.Location = new System.Drawing.Point(15, 285);
            this.printPointButton.Name = "printPointButton";
            this.printPointButton.Size = new System.Drawing.Size(128, 84);
            this.printPointButton.TabIndex = 8;
            this.printPointButton.Text = "In bảng điểm";
            this.printPointButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.printPointButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.printPointButton.UseVisualStyleBackColor = true;
            this.printPointButton.Click += new System.EventHandler(this.printPointButton_Click);
            // 
            // viewScheduleButton
            // 
            this.viewScheduleButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewScheduleButton.Image = ((System.Drawing.Image)(resources.GetObject("viewScheduleButton.Image")));
            this.viewScheduleButton.Location = new System.Drawing.Point(15, 15);
            this.viewScheduleButton.Name = "viewScheduleButton";
            this.viewScheduleButton.Size = new System.Drawing.Size(128, 84);
            this.viewScheduleButton.TabIndex = 5;
            this.viewScheduleButton.Text = "Xem thời khóa biểu";
            this.viewScheduleButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.viewScheduleButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.viewScheduleButton.UseVisualStyleBackColor = true;
            this.viewScheduleButton.Click += new System.EventHandler(this.viewScheduleButton_Click);
            // 
            // viewPointButton
            // 
            this.viewPointButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewPointButton.Image = ((System.Drawing.Image)(resources.GetObject("viewPointButton.Image")));
            this.viewPointButton.Location = new System.Drawing.Point(15, 105);
            this.viewPointButton.Name = "viewPointButton";
            this.viewPointButton.Size = new System.Drawing.Size(128, 84);
            this.viewPointButton.TabIndex = 6;
            this.viewPointButton.Text = "Xem bảng điểm";
            this.viewPointButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.viewPointButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.viewPointButton.UseVisualStyleBackColor = true;
            this.viewPointButton.Click += new System.EventHandler(this.viewPointButton_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.addInfoButton);
            this.panel2.Controls.Add(this.viewInfoButton);
            this.panel2.Controls.Add(this.changeInfoButton);
            this.panel2.Controls.Add(this.deleteInfoButton);
            this.panel2.Location = new System.Drawing.Point(11, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(168, 375);
            this.panel2.TabIndex = 6;
            // 
            // addInfoButton
            // 
            this.addInfoButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addInfoButton.Image = ((System.Drawing.Image)(resources.GetObject("addInfoButton.Image")));
            this.addInfoButton.Location = new System.Drawing.Point(18, 105);
            this.addInfoButton.Name = "addInfoButton";
            this.addInfoButton.Size = new System.Drawing.Size(128, 84);
            this.addInfoButton.TabIndex = 2;
            this.addInfoButton.Text = "Thêm thông tin";
            this.addInfoButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.addInfoButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.addInfoButton.UseVisualStyleBackColor = true;
            this.addInfoButton.Click += new System.EventHandler(this.addInfoButton_Click);
            // 
            // viewInfoButton
            // 
            this.viewInfoButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewInfoButton.Image = ((System.Drawing.Image)(resources.GetObject("viewInfoButton.Image")));
            this.viewInfoButton.Location = new System.Drawing.Point(18, 15);
            this.viewInfoButton.Name = "viewInfoButton";
            this.viewInfoButton.Size = new System.Drawing.Size(128, 84);
            this.viewInfoButton.TabIndex = 1;
            this.viewInfoButton.Text = "Xem thông tin";
            this.viewInfoButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.viewInfoButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.viewInfoButton.UseVisualStyleBackColor = true;
            this.viewInfoButton.Click += new System.EventHandler(this.viewInfoButton_Click);
            // 
            // changeInfoButton
            // 
            this.changeInfoButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeInfoButton.Image = ((System.Drawing.Image)(resources.GetObject("changeInfoButton.Image")));
            this.changeInfoButton.Location = new System.Drawing.Point(18, 195);
            this.changeInfoButton.Name = "changeInfoButton";
            this.changeInfoButton.Size = new System.Drawing.Size(128, 84);
            this.changeInfoButton.TabIndex = 3;
            this.changeInfoButton.Text = "Sửa thông tin";
            this.changeInfoButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.changeInfoButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.changeInfoButton.UseVisualStyleBackColor = true;
            this.changeInfoButton.Click += new System.EventHandler(this.changeInfoButton_Click);
            // 
            // deleteInfoButton
            // 
            this.deleteInfoButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteInfoButton.Image = ((System.Drawing.Image)(resources.GetObject("deleteInfoButton.Image")));
            this.deleteInfoButton.Location = new System.Drawing.Point(18, 285);
            this.deleteInfoButton.Name = "deleteInfoButton";
            this.deleteInfoButton.Size = new System.Drawing.Size(128, 84);
            this.deleteInfoButton.TabIndex = 4;
            this.deleteInfoButton.Text = "Xóa thông tin";
            this.deleteInfoButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.deleteInfoButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.deleteInfoButton.UseVisualStyleBackColor = true;
            this.deleteInfoButton.Click += new System.EventHandler(this.deleteInfoButton_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.currentUserLabel);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Location = new System.Drawing.Point(7, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(546, 38);
            this.panel5.TabIndex = 1;
            // 
            // currentUserLabel
            // 
            this.currentUserLabel.AutoSize = true;
            this.currentUserLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentUserLabel.Location = new System.Drawing.Point(185, 9);
            this.currentUserLabel.Name = "currentUserLabel";
            this.currentUserLabel.Size = new System.Drawing.Size(51, 18);
            this.currentUserLabel.TabIndex = 1;
            this.currentUserLabel.Text = "admin";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Người dùng hiện tại:";
            // 
            // fDashBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 424);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.Name = "fDashBoard";
            this.Text = "Bảng điều khiển";
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.Button viewPointButton;
        private System.Windows.Forms.Button viewScheduleButton;
        private System.Windows.Forms.Button deleteInfoButton;
        private System.Windows.Forms.Button changeInfoButton;
        private System.Windows.Forms.Button viewInfoButton;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button changePasswordButton;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label currentUserLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button addInfoButton;
        private System.Windows.Forms.Button printPointButton;
        private System.Windows.Forms.Button addScheduleButton;
        private System.Windows.Forms.Button modScoreTableButton;
        private System.Windows.Forms.Button viewSubjectButton;
    }
}