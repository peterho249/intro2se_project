﻿using StudentManagementApp.DAO;
using StudentManagementApp.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementApp
{
    public partial class fPrintPointTable : Form
    {

        public fPrintPointTable()
        {
            InitializeComponent();
        }

        bool IdValidation(string userName)
        {
            return AccountDAO.Instance.IdValidation(userName);
        }

        void LoadInfoStudent(string idStu)
        {
            InfoStudent info = InfoStudentDAO.Instance.LoadInfoStudent(idStu);
            dobDateTimePicker.Value = info.Birthday;
            textBoxName.Text = info.Name; 
        }

        public void updateGrid()
        {
            printPointDataGridView.DataSource = ScoreStudentDAO.Instance.LoadAllScoreTable(idStudentTextbox.Text);
        }

        private void idStudentTextbox_TextChanged(object sender, EventArgs e)
        {
            if (idStudentTextbox.Text.Length == 7)
            {
                string mssv = idStudentTextbox.Text;
                if (IdValidation(mssv))
                {
                    printButton.Enabled = true;
                    updateGrid();
                    LoadInfoStudent(idStudentTextbox.Text);
                }
                else
                {
                    MessageBox.Show("Mã số sinh viên không tồn tại", "Thông báo");
                    printButton.Enabled = false;
                }
            }
            else
            { 
                printButton.Enabled = false;
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                PrintDialog _PrintDialog = new PrintDialog();
                PrintDocument _PrintDocument = new PrintDocument();

                _PrintDialog.Document = _PrintDocument; //add the document to the dialog box

                _PrintDocument.PrintPage += new PrintPageEventHandler(_CreateReceipt); 
                DialogResult result = _PrintDialog.ShowDialog();

                if (result == DialogResult.OK)
                {
                    _PrintDocument.Print();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Something went wrong!!");
            }
        }



        private void _CreateReceipt(object sender, PrintPageEventArgs e)
        {
            Graphics graphic = e.Graphics;
            Font font = new Font("Times New Roman", 12);
            float FontHeight = font.GetHeight();
            int startX = 10;
            int startY = 10;
            int offset = 40;


            Pen p = new Pen(Color.Black);
            graphic.DrawLine(p, startX, 180, 835, 180);
            graphic.DrawLine(p, startX, 210, 835, 210);

            graphic.DrawString("          ĐẠI HỌC QUỐC GIA TP HCM                             CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM", new Font("Times New Roman", 12), new SolidBrush(Color.Black), startX, startY);
            graphic.DrawString("TRƯỜNG ĐẠI HỌC KHOA HỌC TỰ NHIÊN                           Độc lập - Tự do - Hạnh phúc", new Font("Times New Roman", 12, FontStyle.Bold), new SolidBrush(Color.Black), startX, 30);
            graphic.DrawString("BẢNG ĐIỂM", new Font("Times New Roman", 28, FontStyle.Bold), new SolidBrush(Color.Blue), 300, 80);
           
            string top = "Họ tên: "+textBoxName.Text.PadRight(25) + "MSSV: "+ idStudentTextbox.Text.PadRight(25) + "Ngày sinh: "+dobDateTimePicker.Text;
            graphic.DrawString(top, new Font("Times New Roman", 12, FontStyle.Bold), new SolidBrush(Color.Black), startX, 120 + offset);

            string top2 = "Mã môn học      Tên môn học                                                                        Học kì                Năm học             Điểm số";
            graphic.DrawString(top2, new Font("Times New Roman", 12, FontStyle.Bold), new SolidBrush(Color.Black), startX, 150 + offset);


            string idSub = "",subj="", sesmeter="", year="", point="";
            
            
            for (int i = 0; i < printPointDataGridView.Rows.Count - 1; i++)
            {
                for(int j=0;j<5;j++)
                {
                    if(j == 0)
                    {
                        idSub += printPointDataGridView.Rows[i].Cells[j].Value.ToString();
                        graphic.DrawString(idSub, new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, 180 + offset);
                    }

                    if (j == 1)
                    {
                        subj += printPointDataGridView.Rows[i].Cells[j].Value.ToString();
                        graphic.DrawString(subj, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 120, 180 + offset);
                    }

                    if (j == 2)
                    {
                        sesmeter += printPointDataGridView.Rows[i].Cells[j].Value.ToString();
                        graphic.DrawString(sesmeter, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 540, 180 + offset);
                    }

                    if (j == 3)
                    {
                        year += printPointDataGridView.Rows[i].Cells[j].Value.ToString();
                        graphic.DrawString(year, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 640, 180 + offset);
                    }

                    if (j == 4)
                    {
                        point += printPointDataGridView.Rows[i].Cells[j].Value.ToString();
                        graphic.DrawString(point, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 780, 180 + offset);
                    }
                    
                }

                graphic.DrawLine(p, startX, 200 + offset, 835, 200 + offset);
                offset = offset + (int)FontHeight + 5; 
                idSub = "";
                subj = "";
                sesmeter = "";
                year = "";
                point = "";
            }
            

            offset = offset + 5; 
            graphic.DrawString("Ngày     Tháng     Năm ", font, new SolidBrush(Color.Black), 550, 180 + offset);
            offset = offset + (int)FontHeight + 5;             
            graphic.DrawString("HIỆU TRƯỞNG ", new Font("Times New Roman", 12, FontStyle.Bold), new SolidBrush(Color.Black), 570, 180 + offset);    
        }
    }
}
