﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Globalization;

namespace StudentManagementApp.DTO
{
    public class ValidateData
    {
        static public bool IsValidDate(DateTime dateStr)
        {
            if (dateStr.Year < 1900 || DateTime.Now.Year - dateStr.Year < 18)
                return false;
            return true;
        }

        static public bool IsValidEmail(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
      
        static public bool IsValidScore(string score)
        {
            var regex = new Regex(@"^[0-9]*(?:\.[0-9]*)?$");
            if (regex.IsMatch(score.ToString()))
            {
                //change to float and check
                float ScoreFloat = float.Parse(score, CultureInfo.InvariantCulture.NumberFormat);
                if (ScoreFloat >= 0 && ScoreFloat <= 10)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
