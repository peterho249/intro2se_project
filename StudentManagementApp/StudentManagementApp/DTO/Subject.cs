﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagementApp.DTO
{
    public class Subject
    {
        private string idSub;
        private string name;

        public Subject(string IdSub, string Name)
        {
            this.idSub = IdSub;
            this.name = Name;
        }

        public Subject(DataRow row)
        {
            this.idSub = (string)row["IdSub"];
            this.name = (string)row["Name"];
        }

        public string IdSub
        {
            get
            {
                return idSub;
            }

            set
            {
                idSub = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
    }
}
