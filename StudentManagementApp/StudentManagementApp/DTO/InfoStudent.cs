﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagementApp.DTO
{
    public class InfoStudent
    {
        private string idStu;
        private string name;
        private DateTime birthday;
        private string phone;
        private string idNo;
        private string address;
        private string email;
        private string sex;

        public InfoStudent(string Idstu, string Name,string Sex, DateTime Birthday, string Phone, string IdNo, string Address, string Email)
        {
            this.address = Address;
            this.birthday = Birthday;
            this.idNo = IdNo;
            this.phone = Phone;
            this.email = Email;
            this.idStu = Idstu;
            this.name = Name;
            this.sex = Sex;
        }

        // Usage:   Convert data row into InfoStudent struct
        // Input:   DataRow to convert
        // Output:  N/A
        public InfoStudent(DataRow row)
        {
            this.idStu = (string)row["IdStu"];
            this.phone = (string)row["PhoneNo"];
            this.name = (string)row["Name"];
            this.address = (string)row["Address"];
            this.email = (string)row["Email"];
            this.idNo = (string)row["IDNo"];
            this.birthday = (DateTime)row["DoB"];
            this.sex = (string)row["Sex"];
        }

        public string IdStu
        {
            get
            {
                return idStu;
            }

            set
            {
                idStu = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public DateTime Birthday
        {
            get
            {
                return birthday;
            }

            set
            {
                birthday = value;
            }
        }

        public string Phone
        {
            get
            {
                return phone;
            }

            set
            {
                phone = value;
            }
        }

        public string IdNo
        {
            get
            {
                return idNo;
            }

            set
            {
                idNo = value;
            }
        }

        public string Address
        {
            get
            {
                return address;
            }

            set
            {
                address = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }
        public string Sex
        {
            get
            {
                return sex;
            }

            set
            {
                sex = value;
            }
        }
    }
}
