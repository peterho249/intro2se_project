﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagementApp.DTO
{
    public class ScoreStudent
    {
        private string idStu;
        private string idSub;
        private int semester;
        private string year;

        public string IdStu { get => idStu; set => idStu = value; }
        public string IdSub { get => idSub; set => idSub = value; }
        public int Semester { get => semester; set => semester = value; }
        public string Year { get => year; set => year = value; }

        public ScoreStudent(string IdStu, string IdSub, int Semester, string Year)
        {
            this.idStu = IdStu;
            this.idSub = IdSub;
            this.semester = Semester;
            this.year = Year;
        }

        public ScoreStudent(DataRow row)
        {
            this.idStu = (string)row["idStu"];
            this.idSub = (string)row["idSub"];
            this.semester = (int)row["Semester"];
            this.year = (string)row["Year"];
        }
    }
}
