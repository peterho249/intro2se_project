﻿using StudentManagementApp.DAO;
using StudentManagementApp.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementApp
{
    public partial class fAddScore : Form
    {
        public fAddScore()
        {
            InitializeComponent();
            this.AcceptButton = okButton;
            LoadSubjectList();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            bool IsValidData = true;
            if (scoreTextbox.Text == "")
            {
                MessageBox.Show("Chưa nhập điểm");
                IsValidData = false;
            }
            //check if score is validated
            if (!ValidateData.IsValidScore(scoreTextbox.Text))
            {
                MessageBox.Show("Điểm không hợp lệ");
                IsValidData = false;
            }
            if (IsValidData)
            {
                string curSubID = ScoreStudentDAO.Instance.GetCurSubID(fViewPoint.curIdStu, fViewPoint.curYear, fViewPoint.curSemester, chooseSubCombo.SelectedValue.ToString());
                //ScoreStudentDAO.Instance.AddScore(fViewPoint.curIdStu, fViewPoint.curSemester, fViewPoint.curYear, curSubID, scoreTextbox.Text);
                MessageBox.Show("Thêm điểm thành công"); this.Close();
            }
        }

        private void LoadSubjectList()
        {
            List<string> subList = ScoreStudentDAO.Instance.GetListSubjectToAdd(fViewPoint.curIdStu, fViewPoint.curYear, fViewPoint.curSemester);
            chooseSubCombo.DataSource = subList;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
