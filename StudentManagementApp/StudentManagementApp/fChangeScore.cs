﻿using StudentManagementApp.DAO;
using StudentManagementApp.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementApp
{
    public partial class fChangeScore : Form
    {
        private static string currentSubID = ""; //current subject id to change
        public fChangeScore()
        {
            InitializeComponent();
            //okButton.Enabled = false;
            this.AcceptButton = okButton;
            LoadSubjectList();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            bool IsValidData = true;
            if (scoreTextbox.Text == "")
            {
                MessageBox.Show("Chưa nhập điểm");
                IsValidData = false;
            }
            //check if score is validated
            if (!ValidateData.IsValidScore(scoreTextbox.Text))
            {
                MessageBox.Show("Điểm không hợp lệ");
                IsValidData = false;
            }
            if (IsValidData)
            {
                ScoreStudentDAO.Instance.UpdateScore(fViewPoint.curIdStu, fViewPoint.curSemester, fViewPoint.curYear, currentSubID, scoreTextbox.Text);
                MessageBox.Show("Cập nhật thành công"); this.Close();
            }
        }

        private void LoadSubjectList()
        {
            List<string> subList = ScoreStudentDAO.Instance.GetListSubject(fViewPoint.curIdStu, fViewPoint.curYear, fViewPoint.curSemester);
            chooseSubCombo.DataSource = subList;
        }

        private void LoadScore()
        {
            string curSubID = ScoreStudentDAO.Instance.GetCurSubID(chooseSubCombo.SelectedValue.ToString());
            currentSubID = curSubID;
            string score = ScoreStudentDAO.Instance.GetCurScore(fViewPoint.curIdStu, fViewPoint.curYear, fViewPoint.curSemester, curSubID);
            scoreTextbox.Text = score;
        }

        private void chooseSubCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;

            if (comboBox.SelectedItem == null) return;

            string selected = comboBox.SelectedItem as string;

            LoadScore();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
