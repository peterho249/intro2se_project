﻿using StudentManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementApp
{
    public partial class fViewSubject : Form
    {
        public fViewSubject()
        {
            InitializeComponent();
        }

        private void addSubjectButton_Click(object sender, EventArgs e)
        {
            Global.type = 1;//Thêm
            fAddSubject form = new fAddSubject();
            form.ShowDialog();
            UpdateGrid();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fViewSubject_Load(object sender, EventArgs e)
        {
            UpdateGrid();
        }

        private void UpdateGrid()
        {
            subjecListDataGridView.DataSource = SubjectDAO.Instance.LoadSubjectList();
        }

        private void changeButton_Click(object sender, EventArgs e)
        {
            Global.type = 2;//sửa
            fAddSubject form = new fAddSubject();
            form.ShowDialog();
            UpdateGrid();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            Global.type = 3;//xóa
            fAddSubject form = new fAddSubject();
            form.ShowDialog();
            UpdateGrid();
        }
    }
}
