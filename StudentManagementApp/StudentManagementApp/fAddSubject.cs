﻿using StudentManagementApp.DAO;
using StudentManagementApp.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementApp
{
    public partial class fAddSubject : Form
    {
        public fAddSubject()
        {
            InitializeComponent();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fAddSubject_Load(object sender, EventArgs e)
        {
            if (Global.type == 1)
            {
                deleteButton.Hide();
                changeButton.Hide();
                this.AcceptButton = addButton;
            }
            else if(Global.type==2)
            {
                addButton.Hide();
                deleteButton.Hide();
                this.AcceptButton = changeButton;
            }
            else if (Global.type == 3)
            {
                addButton.Hide();
                changeButton.Hide();
                this.AcceptButton = deleteButton;
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            bool IsValidData = true;

            //Kiểm tra tên môn học
            if (nameTextbox.Text == "")
            {
                nameError.SetError(nameTextbox, "Lỗi chưa nhập tên môn học");
                IsValidData = false;
            }
            else
                nameError.Clear();

            //Kiểm tra id môn học
            if (idSubTextbox.Text == "")
            {
                idError.SetError(idSubTextbox, "Lỗi chưa nhập id môn học");
                IsValidData = false;
            }
            else
            {
                if (SubjectDAO.Instance.QueryId(idSubTextbox.Text) == true)
                {
                    idError.SetError(idSubTextbox, "ID đã tồn tại");
                    IsValidData = false;
                }
                else
                    idError.Clear();
            }

            if (!IsValidData)
                return;
            if (SubjectDAO.Instance.insertSubject(idSubTextbox.Text, nameTextbox.Text))
            {
                MessageBox.Show("Thêm thành công");
                this.Close();
            }
            else
            {
                MessageBox.Show("Lỗi khi thêm");
            }

        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (SubjectDAO.Instance.DeleteSubject(idSubTextbox.Text))
            {
                MessageBox.Show("Bạn đã xóa thành công");
                this.Close();
            }
            else
                MessageBox.Show("Bạn không thể xóa");

        }

        private void idSubTextbox_TextChanged(object sender, EventArgs e)
        {

            Subject list = SubjectDAO.Instance.LoadSubjecList(idSubTextbox.Text);

            if (list != null)
            {
                idSubTextbox.Text = list.IdSub;
                nameTextbox.Text = list.Name;
            }
            else
            {
                nameTextbox.Clear();
            }
        }

        private void changeButton_Click(object sender, EventArgs e)
        {
            if (SubjectDAO.Instance.ChangeSubject(idSubTextbox.Text,nameTextbox.Text))
            {
                MessageBox.Show("Bạn đã sửa thành công");
                this.Close();
            }
            else
                MessageBox.Show("Bạn không thể sửa");
        }
    }
}
