﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementApp
{
    public partial class fGlobalChangeSchedule : Form
    {
        public fGlobalChangeSchedule()
        {
            InitializeComponent();
        }

		bool CheckSubject(string IdSub)
		{
			DataTable IdSubject = DAO.ScheduleDAO.Instance.ListIdSub();
			foreach (DataRow dr in IdSubject.Rows)
			{
				if ((string)dr[0] == IdSub)
					return true;
			}
			return false;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			int semester = Int32.Parse(chooseSemesterCombobox.Text);
			string year = chooseYearCombobox.Text;

			scheduleDataGridView.DataSource = DAO.ScheduleDAO.Instance.ListScheduleSubject(IdSubjectTextBox.Text, semester, year);
			scheduleDataGridView.Enabled = false;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			if (scheduleDataGridView.Enabled == false)
			{
				scheduleDataGridView.Enabled = true;
			}
			else
			{
				DataTable Schedule = new DataTable();

				// Chuyển thành datatable
				Schedule = ((DataTable)scheduleDataGridView.DataSource).Copy();
				foreach (DataGridViewColumn column in scheduleDataGridView.Columns)
				{
					if (!column.Visible)
					{
						Schedule.Columns.Remove(column.Name);
					}
				}

				// Kiểm tra các môn học có thỏa điều kiện hay không
				foreach (DataRow dr in Schedule.Rows)
				{
					if (CheckSubject((string)dr[0]) == false)
					{
						MessageBox.Show("MÃ MÔN HỌC CỦA BẠN KHÔNG CHÍNH XÁC");
						return;
					}
				}

				// Tiến hành Update thời khóa biểu
				int semester = Int32.Parse(chooseSemesterCombobox.SelectedItem.ToString());
				string year = chooseYearCombobox.SelectedItem.ToString();

				// Xóa toàn bọ thời khóa biểu
				DAO.ScheduleDAO.Instance.DeleteAllItemSubject(IdSubjectTextBox.Text);

				// Đổ dữ liệu lại
				foreach (DataRow dr in Schedule.Rows)
				{

					DAO.ScheduleDAO.Instance.InsertItemSchedule(IdSubjectTextBox.Text, (string)dr[0], semester, year, (string)dr[2]);
				}

				MessageBox.Show("BẠN ĐÃ THAY ĐỔI THỜI KHÓA BIẾU THÀNH CÔNG");
			}
		}

		private void comboBoxSemester_SelectedIndexChanged(object sender, EventArgs e)
		{

		}
	}
}
