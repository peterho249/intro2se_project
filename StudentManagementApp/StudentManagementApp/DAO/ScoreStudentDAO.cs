﻿using StudentManagementApp.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagementApp.DAO
{
    public class ScoreStudentDAO
    {
        private static ScoreStudentDAO instance;

        public static ScoreStudentDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ScoreStudentDAO();
                }
                return instance;
            }
            private set
            {
                instance = value;
            }
        }

        private ScoreStudentDAO() { }

        //load score to use for editing
        public ScoreStudent LoadScore(string studentID, int semester, string year)
        {
            string query = "USP_GetScore @MSSV , @Semester , @Year";
            DataTable result = DataProvider.Instance.ExecuteQuery(query, new object[] { studentID, semester, year });
            ScoreStudent score = new ScoreStudent(result.Rows[0]);
            return score;
        }


        //load score for table showing
        public DataTable LoadScoreTable(string studentID, int semester, string year)
        {
            string query = "USP_GetScore @MSSV , @Semester , @Year";

            DataTable result = DataProvider.Instance.ExecuteQuery(query, new object[] { studentID, semester, year });
            return result;
        }

        //load year list for combobox 
        public List<string> GetListYear(string idstu)
        {
            List<string> list = new List<string>();
            string query = @"select SCHEDULEPOINT.Year from SCHEDULEPOINT where SCHEDULEPOINT.idStu = '" + idstu + "' group by SCHEDULEPOINT.Year";
            DataTable data = DataProvider.Instance.ExecuteQuery(query);

            foreach (DataRow item in data.Rows)
            {
                list.Add(item["Year"].ToString());
            }
            return list;
        }

        //load semester list for combobox 
        public List<string> GetListSemester(string year)
        {
            List<string> list = new List<string>();
            string query = @"select SCHEDULEPOINT.Semester from SCHEDULEPOINT where SCHEDULEPOINT.Year = '" + year + "' group by SCHEDULEPOINT.Semester";
            DataTable data = DataProvider.Instance.ExecuteQuery(query);

            foreach (DataRow item in data.Rows)
            {
                list.Add(item["Semester"].ToString());
            }
            return list;
        }

        //load subject list for combobox
        public List<string> GetListSubject(string idstu, string year, string semester)
        {
            List<string> list = new List<string>();
            string query = @"select SUBJECTLIST.Name from SCHEDULEPOINT, SUBJECTLIST where SCHEDULEPOINT.idStu = '" + idstu + "' and SCHEDULEPOINT.Year = '" + year + "' and SCHEDULEPOINT.Semester = " + semester + " and SUBJECTLIST.IdSub = SCHEDULEPOINT.idSub group by SUBJECTLIST.Name";
            DataTable data = DataProvider.Instance.ExecuteQuery(query);
            foreach (DataRow item in data.Rows)
            {
                list.Add(item["Name"].ToString());
            }
            return list;
        }


        //load year list to edit combobox 
        public List<string> GetListYearEdit()
        {
            List<string> list = new List<string>();
            string query = @"select SCHEDULEPOINT.Year from SCHEDULEPOINT group by SCHEDULEPOINT.Year";
            DataTable data = DataProvider.Instance.ExecuteQuery(query);

            foreach (DataRow item in data.Rows)
            {
                list.Add(item["Year"].ToString());
            }
            return list;
        }

        //load subject to edit score for combobox
        public List<string> GetListSubjectEdit(string year, string semester)
        {
            List<string> list = new List<string>();
            string query = @"select SUBJECTLIST.Name from SCHEDULEPOINT, SUBJECTLIST where SCHEDULEPOINT.Year = '" + year + "' and SCHEDULEPOINT.Semester = " + semester + " and SUBJECTLIST.IdSub = SCHEDULEPOINT.idSub group by SUBJECTLIST.Name";
            DataTable data = DataProvider.Instance.ExecuteQuery(query);
            foreach (DataRow item in data.Rows)
            {
                list.Add(item["Name"].ToString());
            }
            return list;
        }
        //load score to edit directly
        public DataTable LoadScoreTableDirectEdit(string idsub, int semester, string year)
        {
            string query = "USP_LoadListStuSub @SubName , @Semester , @Year";

            DataTable result = DataProvider.Instance.ExecuteQuery(query, new object[] {idsub, semester, year });
            return result;
        }

        //load current sub ID to change
        public string GetCurSubID(string sub)
        {
            string query = @"select SUBJECTLIST.IdSub from SUBJECTLIST where SUBJECTLIST.Name = N'" + sub + "'";
            DataTable data = DataProvider.Instance.ExecuteQuery(query);
            return data.Rows[0]["idSub"].ToString();
        }
        // load current score
        public string GetCurScore(string idstu, string year, string semester, string subId)
        {
            string query = @"select SCHEDULEPOINT.Point from SCHEDULEPOINT where SCHEDULEPOINT.idStu = '" + idstu + "' and SCHEDULEPOINT.Year = '" + year + "' and SCHEDULEPOINT.Semester = " + semester + " and SCHEDULEPOINT.idSub ='" + subId + "' group by SCHEDULEPOINT.Point";
            DataTable data = DataProvider.Instance.ExecuteQuery(query);
            return data.Rows[0]["Point"].ToString();
        }
        //change score
        public bool UpdateScore(string studentID, string semester, string year, string subID, string newScore)
        {
            int semesterInt = Int32.Parse(semester);

            float newScoreFloat = float.Parse(newScore, CultureInfo.InvariantCulture.NumberFormat);

            string query = "USP_ChangeScore @MSSV , @Semester , @Year , @IdSub , @Point";

            int result = DataProvider.Instance.ExecuteNonQuery(query, new object[] { studentID, semesterInt, year, subID, newScoreFloat});
            if (result != 0)
                return true;
            else
                return false;
        }

        //load all score for score priting
        public DataTable LoadAllScoreTable(string studentID)
        {
            string query = "USP_GetScore_All @MSSV";

            DataTable result = DataProvider.Instance.ExecuteQuery(query, new object[] { studentID });
            return result;
        }
    }
}
