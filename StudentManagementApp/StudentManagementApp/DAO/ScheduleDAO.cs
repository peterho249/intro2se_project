﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagementApp.DAO
{
    class ScheduleDAO
    {
        private static ScheduleDAO instance;

        public int type = 0;

        public static ScheduleDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new ScheduleDAO();
                return instance;
            }

            private set
            {
                instance = value;
            }
        }

        private ScheduleDAO() { }

        //--------------------------------
        public DataTable ListSchedule(string username, int HK, string year)
        {
            List<string> list = new List<string>();
            string query = "USP_QuerySchedule @username , @HK , @year ";
            DataTable result = DataProvider.Instance.ExecuteQuery(query, new object[] { username, HK, year });
            return result;
        }

		public DataTable ListScheduleSubject(string idSub, int HK, string year)
        {
            List<string> list = new List<string>();
            string query = "USP_QueryScheduleSubject @idSub , @HK , @year ";
			DataTable result = DataProvider.Instance.ExecuteQuery(query, new object[] { idSub, HK, year });
			return result;
		}

        public List<string> ListYear()
        {
            List<string> list = new List<string>();
            string query = "USP_QueryYear";
            DataTable data= DataProvider.Instance.ExecuteQuery(query);

            foreach (DataRow item in data.Rows)
            {
                list.Add(item["Year"].ToString());
            }
            return list;
        }

        public List<string> ListSemesterForSubject(string idSub, string year)
        {
            List<string> list = new List<string>();
            string query = "USP_ListSemesterSub @idSub , @year";
            DataTable data = DataProvider.Instance.ExecuteQuery(query, new object[] { idSub, year });

            foreach (DataRow item in data.Rows)
            {
                list.Add(item["Semester"].ToString());
            }
            return list;
        }

        public List<string> ListSemesterForStudent(string idStu, string year)
        {
            List<string> list = new List<string>();
            string query = "USP_ListSemesterStu @idStu , @year";
            DataTable data = DataProvider.Instance.ExecuteQuery(query, new object[] { idStu, year });

            foreach (DataRow item in data.Rows)
            {
                list.Add(item["Semester"].ToString());
            }
            return list;
        }

        public List<string> ListIdSub()
        {
            List<string> list = new List<string>();
            DataTable data = DataProvider.Instance.ExecuteQuery("SELECT DISTINCT IdSub FROM SUBJECTLIST");

            foreach (DataRow item in data.Rows)
            {
                list.Add(item[0].ToString());
            }
            return list;
        }

        // Kiểm tra xem mục đó trong thời khóa biểu đã có hay chưa
        public bool CheckItemSchedule(string idStu, string idSub, int HK, string year)
        {
            string query = "USP_CheckItemSchedule @idStd , @idSub , @HK , @year";
            DataTable tam = DataProvider.Instance.ExecuteQuery(query, new object[] { idStu, idSub, HK, year });
            if (tam.Rows.Count > 0)
            {
                return true;
            }
            else
                return false;
        }

        public DataTable UpdateItemSchedule(string idStu, string idSub, int HK, string year, string period)
        {
            string query = "USP_UpdateItemSchedule @idStu , @idSub , @HK , @year , @period";
            return DataProvider.Instance.ExecuteQuery(query, new object[] { idStu, idSub, HK, year, period });
        }

        public DataTable InsertItemSchedule(string idStu, string idSub, int HK, string year, string period)
        {
            string query = "USP_InsertItemSchedule @idStu , @idSub , @HK , @year , @period";
            return DataProvider.Instance.ExecuteQuery(query, new object[] { idStu, idSub, HK, year, period });
        }

        public bool DeleteAll(string idStu)
        {
            string query = "USP_DeleteSchedule @idStu ";
            DataProvider.Instance.ExecuteQuery(query, new object[] { idStu });
            return true;
        }

		public bool DeleteAllItemSubject(string idSub)
		{
			string query = "USP_DeleteScheduleSubject @idSub ";
			DataProvider.Instance.ExecuteQuery(query, new object[] { idSub });
			return true;
		}
	}
}
