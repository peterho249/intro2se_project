﻿using StudentManagementApp.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagementApp.DAO
{
    public class InfoStudentDAO
    {
        private static InfoStudentDAO instance;

        public static InfoStudentDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new InfoStudentDAO();
                return instance;
            }

            private set
            {
                instance = value;
            }
        }
        private InfoStudentDAO() { }

        // Usage:   Load data of all student
        // Input:   N/A
        // Output:  data from STUDENT table as DataTable
        public DataTable LoadInfoStudent()
        {
            return DataProvider.Instance.ExecuteQuery("USP_GetInfoStudentList");
        }

        // Usage:   Load data of 1 student
        // Input:   Id of student
        // Output:  data of 1 student as InfoStudent struct
        public InfoStudent LoadInfoStudent(string IDStudent)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetInfoCurrentStudent @IdStu", new object[]{ IDStudent });

            if (data.Rows.Count == 0)
                return null;

            InfoStudent info = new InfoStudent(data.Rows[0]);
            return info;
        }

        public bool QueryId(string userName)
        {
            String query = "USP_QueryId @userName";
            DataTable result = DataProvider.Instance.ExecuteQuery(query, new object[] { userName });
            return result.Rows.Count > 0;
        }

        public bool QueryIdNo(string idNo)
        {
            String query = "USP_QueryCMND @idNo";
            DataTable result = DataProvider.Instance.ExecuteQuery(query, new object[] { idNo });
            return result.Rows.Count > 0;
        }

        public bool insertStudent(string id, string name,string sex, DateTime date, string phone, string idNo, string address, string email)
        {
            string query = "USP_AddInfoStudent @username , @HoTen , @GioiTinh , @NgaySinh , @SDT , @CMND , @DiaChi , @Email";
            int result = DataProvider.Instance.ExecuteNonQuery(query, new object[] { id, name, sex, date, phone, idNo, address, email });
            return result > 0;
        }
    }
}
