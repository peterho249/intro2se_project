﻿using StudentManagementApp.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagementApp.DAO
{
    public class SubjectDAO
    {
        private static SubjectDAO instance;

        public static SubjectDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new SubjectDAO();
                return instance;
            }

            private set
            {
                instance = value;
            }
        }
        private SubjectDAO() { }

        public DataTable LoadSubjectList()
        {
            return DataProvider.Instance.ExecuteQuery("USP_GetSubjectList");
        }

        // Usage:   Load data of 1 student
        // Input:   Id of student
        // Output:  data of 1 student as InfoStudent struct

        //CHUA 
        public Subject LoadSubjecList(string IdSub)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetInfoCurrentSubject @IdSub", new object[] { IdSub });

            if (data.Rows.Count == 0)
                return null;

            Subject list = new Subject(data.Rows[0]);
            return list;
        }

        public bool QueryId(string idSub)
        {
            String query = "USP_QueryIdSub @idSub";
            DataTable result = DataProvider.Instance.ExecuteQuery(query, new object[] { idSub });
            return result.Rows.Count > 0;
        }

        //public bool QueryIdNo(string idNo)
        //{
        //    String query = "USP_QueryCMND @idNo";
        //    DataTable result = DataProvider.Instance.ExecuteQuery(query, new object[] { idNo });
        //    return result.Rows.Count > 0;
        //}

        public bool insertSubject(string id, string name)
        {
            string query = "USP_AddSubject @idSub , @Name";
            int result = DataProvider.Instance.ExecuteNonQuery(query, new object[] { id, name });
            return result > 0;
        }

        public bool DeleteSubject(string idSub)
        {
            string query = "USP_DeleteSubject @idSub ";
            int result = DataProvider.Instance.ExecuteNonQuery(query, new object[] { idSub });
            if (result != 0)
                return true;
            else
                return false;
        }

        public bool ChangeSubject(string idSub,string name)
        {
            string query = "USP_ChangeSubject @idSub , @name";
            int result = DataProvider.Instance.ExecuteNonQuery(query, new object[] { idSub,name });
            if (result != 0)
                return true;
            else
                return false;
        }

    }
}
