﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagementApp.DAO
{
    public class AccountDAO
    {
        private static AccountDAO instance;

        public int type = 0;

        public static AccountDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new AccountDAO();
                return instance;
            }

            private set
            {
                instance = value;
            }
        }
        private AccountDAO() { }

        // Usage: check login infor
        // Input:   userName, passWord
        // Output:  true if login succeed, else false
        public bool Login(string userName, string passWord)
        {
            String query = "USP_Login @userName , @passWord";
            DataTable result = DataProvider.Instance.ExecuteQuery(query, new object[] { userName, passWord });
            if (result.Rows.Count > 0)
            {
                type = (int)result.Rows[0]["type"];
                return true;
            }
            else
                return false;
        }
        // Usage: check id validation
        // Input:   userName
        // Output:  true if id exists, else false
        public bool IdValidation(string userName)
        {
            String query = "USP_IDvalidation @userName";
            DataTable result = DataProvider.Instance.ExecuteQuery(query, new object[] { userName});
            if (result.Rows.Count > 0)
            {
                type = (int)result.Rows[0]["type"];
                return true;
            }
            else
                return false;
        }

        // Usage:   Change password of account
        // Input:   username, password, newpassword
        // Output:  true if change password succeed, else false
        public bool ChangePassword(string userName, string passWord, string newPassword)
        {
            string query = "USP_ChangePassword @username , @password , @newpassword";
            int result = DataProvider.Instance.ExecuteNonQuery(query, new object[] { userName, passWord, newPassword });
            if (result != 0)
                return true;
            else
                return false;
        }

        public bool ChangeInfo(string userName, string HoTen,string GioiTinh,DateTime NgaySinh, string SDT, string CMND,string DiaChi, string Email)
        {
            string query = "USP_ChangeInfo @username , @HoTen , @GioiTinh , @NgaySinh , @SDT , @CMND , @DiaChi , @Email";
            int result = DataProvider.Instance.ExecuteNonQuery(query, new object[] { userName, HoTen, GioiTinh, NgaySinh, SDT, CMND, DiaChi, Email });
            if (result != 0)
                return true;
            else
                return false;
        }

        public bool DeleteMember(string userName)
        {
            string query = "USP_DeleteMember @username ";
            int result = DataProvider.Instance.ExecuteNonQuery(query, new object[] { userName });
            if (result != 0)
                return true;
            else
                return false;
        }

        public bool insertAccount(string id)
        {
            string query= "USP_AddAccount @username";
            int result = DataProvider.Instance.ExecuteNonQuery(query, new object[] { id});
            return true;
        }

    }
}
