﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementApp
{
    public partial class fAddSchedule : Form
    {
        public fAddSchedule()
        {
            InitializeComponent();

            // Khởi tạo dữ liệu cho ComboBox IdSubject
            subjectCombobox.Items.Clear();
            List<string> IdSubList = DAO.ScheduleDAO.Instance.ListIdSub();
            subjectCombobox.DataSource = IdSubList;

            // Khởi tạo dữ liệu cho 2 ComboBox còn lại
            IniChooseYearCombobox(subjectCombobox.Text);

            UpdateGrid();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        bool CheckSubject(string IdSub)
        {
            List<string> yearList = DAO.ScheduleDAO.Instance.ListIdSub();
            foreach (string dr in yearList)
            {
                if (dr == IdSub)
                    return true;
            }
            return false;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            DataTable Schedule = new DataTable();

            // Chuyển thành datatable
            Schedule = ((DataTable)listStuGrid.DataSource).Copy();
            foreach (DataGridViewColumn column in listStuGrid.Columns)
            {
                if (!column.Visible)
                {
                    Schedule.Columns.Remove(column.Name);
                }
            }

            // Kiểm tra các Id học sinh có thỏa điều kiện hay không
            foreach (DataRow dr in Schedule.Rows)
            {
                if (DAO.InfoStudentDAO.Instance.QueryId((string)dr[0]) == false)
                {
                    MessageBox.Show("MÃ SINH VIÊN MỚI THÊM KHÔNG CHÍNH XÁC!");
                    return;
                }
            }

            // Tiến hành Update thời khóa biểu
            int semester = Int32.Parse(chooseSemesterCombobox.SelectedItem.ToString());
            string year = chooseYearCombobox.SelectedItem.ToString();

            // Xóa toàn bọ thời khóa biểu
            DAO.ScheduleDAO.Instance.DeleteAllItemSubject(subjectCombobox.Text);

            // Đổ dữ liệu lại
            foreach (DataRow dr in Schedule.Rows)
            {
                DAO.ScheduleDAO.Instance.InsertItemSchedule( (string)dr[0], subjectCombobox.Text, semester, year, (string)dr[2]);
            }

            MessageBox.Show("BẠN ĐÃ THAY ĐỔI THỜI KHÓA BIẾU THÀNH CÔNG");
            
        }

        public void IniChooseYearCombobox(string idSub)
        {
            // Load dữ liệu cho ComboBox Year
            chooseYearCombobox.Items.Clear();
            List<string> yearList = DAO.ScheduleDAO.Instance.ListYear();
            chooseYearCombobox.DataSource = yearList;

            // Load dữ liệu cho ComboBox Semester
            chooseSemesterCombobox.Items.Clear();
            yearList = DAO.ScheduleDAO.Instance.ListSemesterForSubject(idSub, chooseYearCombobox.SelectedItem.ToString());
            if (yearList.Count == 0)
                yearList.Add("1");
            chooseSemesterCombobox.DataSource = yearList;
        }

        private void subjectCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateGrid();
        }

        private void UpdateGrid()
        {
            int semester = Int32.Parse(chooseSemesterCombobox.Text);
            string year = chooseYearCombobox.Text;

            if (IsValidSemester(chooseSemesterCombobox.Text) && IsValidYear(chooseYearCombobox.Text) &&
                CheckSubject(subjectCombobox.Text))
            {
                listStuGrid.DataSource = DAO.ScheduleDAO.Instance.ListScheduleSubject(subjectCombobox.Text, semester, year);
                listStuGrid.Enabled = true;
            }
        }

        private void chooseYearCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateGrid();
        }

        private void chooseSemesterCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateGrid();
        }

        private void subjectCombobox_TextUpdate(object sender, EventArgs e)
        {
            if (CheckSubject(subjectCombobox.Text) == true)
                UpdateGrid();
        }

        private void chooseYearCombobox_TextUpdate(object sender, EventArgs e)
        {
            if (IsValidYear(chooseYearCombobox.Text))
                UpdateGrid();
        }

        private bool IsValidYear(string year)
        {
            var token = year.Split('-');
            if (token.Length != 2)
                return false;
            int firstYear, lastYear;
            if (Int32.TryParse(token[0], out firstYear) == false ||
                Int32.TryParse(token[1], out lastYear) == false)
                return false;

            if (lastYear - firstYear != 1)
                return false;

            return true;
        }

        private bool IsValidSemester(string semester)
        {
            int sem;
            if (Int32.TryParse(semester, out sem) == false)
                return false;

            if (sem < 1 || sem > 3)
                return false;

            return true;

        }

        private void chooseSemesterCombobox_TextUpdate(object sender, EventArgs e)
        {
            if (IsValidSemester(chooseSemesterCombobox.Text))
                UpdateGrid();
        }
    }
}
