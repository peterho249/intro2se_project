﻿using StudentManagementApp.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementApp
{
    
    public partial class fLogIn : Form
    {
        public fLogIn()
        {
            InitializeComponent();
        }

        bool Login(string userName, string passWord)
        {
            return AccountDAO.Instance.Login(userName, passWord);
        }

        private void fLogIn_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            string userName = usernameTextbox.Text;
            string passWord = passwordTextbox.Text;
            if (Login(userName, passWord))
            {
                if (AccountDAO.Instance.type==1)
                    Global.adminFlag = true;
                else
                    Global.adminFlag = false;

                Global.userName = userName;
                usernameTextbox.Text = "";
                passwordTextbox.Text = "";
                fDashBoard f = new fDashBoard();
                this.Hide();
                f.ShowDialog();
                this.Show();
            }
            else
            {
                MessageBox.Show("Sai tên tài khoản hoặc mật khẩu", "Thông báo");
            }
        }

        private void fLogIn_Load(object sender, EventArgs e)
        {

        }
    }
}
