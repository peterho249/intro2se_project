﻿using StudentManagementApp.DAO;
using StudentManagementApp.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementApp
{
    public partial class fDeleteInfo : Form
    {
        public fDeleteInfo()
        {
            InitializeComponent();
            okButton.Enabled = false;
        }

        private void studentIdTextbox_TextChanged(object sender, EventArgs e)
        {
            if(studentIdTextbox.Text.Length == 7)
            {
                LoadInfoStudent(studentIdTextbox.Text);
                okButton.Enabled = true;
            }
            else
            {
                nameTextbox.Clear();
                emailTextbox.Clear();
                phoneNoTextbox.Clear();
                idNoTextbox.Clear();
                addressTextbox.Clear();
                dobTextbox.Clear();
                sexTextBox.Clear();
            }
        }

        void LoadInfoStudent(string idStu)
        {
            InfoStudent info = InfoStudentDAO.Instance.LoadInfoStudent(idStu);

            if (info == null)
                return;

            studentIdTextbox.Text = info.IdStu;
            nameTextbox.Text = info.Name;
            emailTextbox.Text = info.Email;
            phoneNoTextbox.Text = info.Phone;
            idNoTextbox.Text = info.IdNo;
            addressTextbox.Text = info.Address;
            dobTextbox.Text = Convert.ToDateTime(info.Birthday).ToString("dd/MM/yyyy");
            sexTextBox.Text = info.Sex;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (AccountDAO.Instance.DeleteMember(studentIdTextbox.Text))
            {
                MessageBox.Show("Bạn đã xóa thành công");
                this.Close();
            }
            else
                MessageBox.Show("Bạn không thể xóa");
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
