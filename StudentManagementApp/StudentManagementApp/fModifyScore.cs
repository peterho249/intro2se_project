﻿using StudentManagementApp.DAO;
using StudentManagementApp.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentManagementApp
{
    public partial class fModifyScore : Form
    {
        public static string curYear = "";
        public static string curSemester = "";
        public static string subId = "";


        public fModifyScore()
        {
            InitializeComponent();
            loadYearList();
            
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in listStuGrid.Rows)
            {
                if (row.Cells["Id"].Value != null && row.Cells["Điểm"].Value != null && row.Cells["Điểm"].Value.ToString() != "")
                {
                    if (ValidateData.IsValidScore(row.Cells["Điểm"].Value.ToString()))
                    {
                        {
                            UpdateScore(row.Cells["Id"].Value.ToString(), curSemester, curYear, subId, row.Cells["Điểm"].Value.ToString());
                        }
                    }
                }
            }
            MessageBox.Show("Chỉnh sửa điểm thành công!", "Thông báo");
            updateGrid();
        }

        private void LoadSubjectList()
        {
            List<string> subList = ScoreStudentDAO.Instance.GetListSubjectEdit(curYear, curSemester);
            subjectCombobox.DataSource = subList;
            updateGrid();
        }

        private void loadYearList()
        {
            List<string> yearList = ScoreStudentDAO.Instance.GetListYearEdit();
            chooseYearCombobox.DataSource = yearList;
        }

        private void loadSemesterListByYear(string year)
        {
            List<string> semesterList = ScoreStudentDAO.Instance.GetListSemester(year);
            chooseSemesterCombobox.DataSource = semesterList;
        }

        private void chooseYearCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;

            if (comboBox.SelectedItem == null) return;

            string selected = comboBox.SelectedItem as string;

            fModifyScore.curYear = selected;
            loadSemesterListByYear(selected);
        }

        private void chooseSemesterCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;

            if (comboBox.SelectedItem == null) return;

            string selected = comboBox.SelectedItem as string;
            fModifyScore.curSemester = selected;
            LoadSubjectList();
        }

        public void updateGrid()
        {
            int semester = Int32.Parse(chooseSemesterCombobox.SelectedItem.ToString());
            string year = chooseYearCombobox.SelectedItem.ToString();
            string curSubID = ScoreStudentDAO.Instance.GetCurSubID(subjectCombobox.SelectedValue.ToString());
            fModifyScore.subId = curSubID;

            listStuGrid.DataSource = ScoreStudentDAO.Instance.LoadScoreTableDirectEdit(subId, semester, year);
        }

        private void subjectCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateGrid();
        }

        private void listStuGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        //change score
        public bool UpdateScore(string studentID, string semester, string year, string subID, string newScore)
        {
            int semesterInt = Int32.Parse(semester);

            float newScoreFloat = float.Parse(newScore, CultureInfo.InvariantCulture.NumberFormat);

            string query = "USP_ChangeScore @MSSV , @Semester , @Year , @IdSub , @Point";

            int result = DataProvider.Instance.ExecuteNonQuery(query, new object[] { studentID, semesterInt, year, subID, newScoreFloat });
            if (result != 0)
                return true;
            else
                return false;
        }
    }
}
