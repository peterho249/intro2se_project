﻿use [master]
go
if DB_ID('StudentMan') is not null
	drop database StudentMan
go

create database StudentMan
go

use StudentMan
go

-- Tao bang danh sach hoc sinh
create table STUDENT
(
	IdStu	varchar(10),
	Name	nvarchar(50),
	Sex		nvarchar(5),
	DoB		date,
	PhoneNo	varchar(12),
	IDNo	varchar(10) unique,
	Address	nvarchar(100),
	Email	varchar(50),
	primary key (IdStu)
)
go


-- Tao bang danh sach tai khoan
create table ACCOUNT
(
	Username	varchar(10),
	Password	varchar(20),
	type		int,		-- 0: Sinh vien, 1: Admin
	primary key (Username)
)
go

-- Tao bang danh sach mon hoc
create table SUBJECTLIST
(
	IdSub		varchar(10),
	Name		nvarchar(50),
	primary key (IdSub)
)
go

-- Tao bang thoi khoa bieu
create table SCHEDULEPOINT
(
	idStu		varchar(10),
	idSub		varchar(10),
	Semester	int,
	Year		varchar(15),
	Period		varchar(10),
	Point		float,
	primary key (IdStu, IdSub, Semester, Year)
)
go

-- Tham chieu ma sinh vien tu bang thoi khoa bieu den bang danh sach sinh vien
alter table SCHEDULEPOINT
add constraint FK_SchedulePoint_Student
foreign key (IdStu)
references STUDENT(IdStu)
go

-- Tham chieu ma mon hoc tu bang thoi khoa bieu den bang danh sach mon hoc
alter table SCHEDULEPOINT
add constraint FK_Schedule_SubjectList
foreign key (IdSub)
references SUBJECTLIST(IdSub)
go

-- Tham chieu thong tin dang ky mon hoc cua sinh vien

---Nhâp liệu
--Mô tả nội dung
--Có 3 sinh viên trong danh sách sinh viên
--Sinh viên có tài khoản và mật khẩu giống nhau đều là MSSV
--Admin có tài khoản là 'admin' và mật khẩu là 'administrator' vì chuỗi không lưu được quá 10 ký tự nên mật khẩu và tài khoản phải khác nhau
--Thời khóa biểu: có 2 thời khóa biểu của năm học trước và 1 thời khóa biểu của học kỳ đang học
--Điểm số: các môn học đã học đều được thêm điểm, các môn đang học thì có giá trị 'null'
--Danh sách môn học: có 15 môn học, thứ tự môn học trong thời khóa biểu sẽ lấy từ trên xuống

----------------------------STUDENT----------------------------

--select * from STUDENT

insert into STUDENT
values('1512043',N'Cao Thanh Sang',N'Nam','1997-02-18','01234567891','485269823',
N'40 Bà Huyện Thanh Quan, Phường 06, Quận 3, TP Hồ Chí Minh','a@gmail.com')
insert into STUDENT
values('1512055',N'Trần Thanh Triều',N'Nam','1997-06-03','01234567242','054823688',
N'Võ Văn Kiệt, Phường Cầu Kho, Quận 1, TP Hồ Chí Minh','ab@gmail.com')
insert into STUDENT
values('1512123',N'Võ Phương Sa',N'Nữ','1997-09-18','01234567256','865269413',
N'53 Tân Hòa Đông, Phường 14, Quận 6, TP Hồ Chí Minh','abc@gmail.com')

----------------------------ACCOUNT----------------------------

--select * from ACCOUNT

insert into ACCOUNT
values('1512043','1512043',0)
insert into ACCOUNT
values('1512055','1512055',0)
insert into ACCOUNT
values('1512123','1512123',0)
--admin
insert into ACCOUNT
values('admin','admin',1)

----------------------------SUBJECTLIST----------------------------

--select * from SUBJECTLIST

insert into SUBJECTLIST
values('CTT006',N'Phương pháp lập trình hướng đối tượng')
insert into SUBJECTLIST
values('XHH001',N'Tâm lý đại cương')
insert into SUBJECTLIST
values('TTH045',N'Xác suất thống kê')
insert into SUBJECTLIST
values('VLH005',N'Vật lý 1')
insert into SUBJECTLIST
values('TTH046',N'Toán học tổ hợp')
insert into SUBJECTLIST
values('CTT101',N'Cấu trúc dữ liệu và giải thuật')
insert into SUBJECTLIST
values('BAA00002',N'Đường lối của Đảng Cộng sản Việt Nam')
insert into SUBJECTLIST
values('CTT104',N'Kiến trúc máy tính và hợp ngữ')
insert into SUBJECTLIST
values('CTH001',N'Những nguyên lý cơ bản của chủ nghĩa Mac-LeNin')
insert into SUBJECTLIST 
values('BAA00003',N'Tư tưởng Hồ Chí Minh')
insert into SUBJECTLIST
values('CTT102',N'Cơ sở dữ liệu')
insert into SUBJECTLIST
values('CTT103',N'Hệ điều hành')
insert into SUBJECTLIST
values('CTT501',N'Lập trình Windows')
insert into SUBJECTLIST
values('CTT105',N'Mạng máy tính')
insert into SUBJECTLIST
values('CTT502',N'Nhập môn công nghệ phần mềm')

----------------------------SCHEDULEPOINT----------------------------

--select * from SCHEDULEPOINT

--SV_1
--hk1
insert into SCHEDULEPOINT
values('1512043','BAA00002',1,'2016-2017','T2(1-3)', 9.5)
insert into SCHEDULEPOINT
values('1512043','BAA00003',1,'2016-2017','T3(1-3)', 6.5)
insert into SCHEDULEPOINT
values('1512043','CTH001',1,'2016-2017','T5(3-6)', 8)
insert into SCHEDULEPOINT
values('1512043','CTT006',1,'2016-2017','T7(8-10)', 10)
--hk2
insert into SCHEDULEPOINT
values('1512043','CTT101',2,'2016-2017','T3(8-10)', 9)
insert into SCHEDULEPOINT
values('1512043','CTT102',2,'2016-2017','T5(3-6)', 8)
insert into SCHEDULEPOINT
values('1512043','CTT103',2,'2016-2017','T4(1-3)', 6)
insert into SCHEDULEPOINT
values('1512043','CTT104',2,'2016-2017','T6(1-3)', 7)
--hk1-nam nay
insert into SCHEDULEPOINT
values('1512043','CTT105',1,'2017-2018','T2(1-3)', null)
insert into SCHEDULEPOINT
values('1512043','CTT501',1,'2017-2018','T7(1-3)', null)
insert into SCHEDULEPOINT
values('1512043','CTT502',1,'2017-2018','T5(3-6)', null)
insert into SCHEDULEPOINT
values('1512043','TTH045',1,'2017-2018','T4(1-3)', null)

--SV_2
--hk1
insert into SCHEDULEPOINT
values('1512055','BAA00002',1,'2016-2017','T2(1-3)', 9.8)
insert into SCHEDULEPOINT
values('1512055','BAA00003',1,'2016-2017','T3(1-3)', 8.5)
insert into SCHEDULEPOINT
values('1512055','CTH001',1,'2016-2017','T5(3-6)', 9)
insert into SCHEDULEPOINT
values('1512055','CTT006',1,'2016-2017','T7(8-10)', 10)
--hk2
insert into SCHEDULEPOINT
values('1512055','CTT101',2,'2016-2017','T3(8-10)', 6.5)
insert into SCHEDULEPOINT
values('1512055','CTT102',2,'2016-2017','T5(3-6)', 9.5)
insert into SCHEDULEPOINT
values('1512055','CTT103',2,'2016-2017','T4(1-3)', 9)
insert into SCHEDULEPOINT
values('1512055','CTT104',2,'2016-2017','T6(1-3)', 8)
--hk1-nam nay
insert into SCHEDULEPOINT
values('1512055','CTT105',1,'2017-2018','T2(1-3)', null)
insert into SCHEDULEPOINT
values('1512055','CTT501',1,'2017-2018','T7(1-3)', null)
insert into SCHEDULEPOINT
values('1512055','CTT502',1,'2017-2018','T5(3-6)', null)
insert into SCHEDULEPOINT
values('1512055','TTH045',1,'2017-2018','T4(1-3)', null)

--SV_3
--hk1
insert into SCHEDULEPOINT
values('1512123','BAA00002',1,'2016-2017','T2(1-3)', 8)
insert into SCHEDULEPOINT
values('1512123','BAA00003',1,'2016-2017','T3(1-3)', 7)
insert into SCHEDULEPOINT
values('1512123','CTH001',1,'2016-2017','T5(3-6)', 10)
insert into SCHEDULEPOINT
values('1512123','CTT006',1,'2016-2017','T7(8-10)', 8.5)
--hk2
insert into SCHEDULEPOINT
values('1512123','CTT101',2,'2016-2017','T3(8-10)', 9)
insert into SCHEDULEPOINT
values('1512123','CTT102',2,'2016-2017','T5(3-6)', 9)
insert into SCHEDULEPOINT
values('1512123','CTT103',2,'2016-2017','T4(1-3)', 8.5)
insert into SCHEDULEPOINT
values('1512123','CTT104',2,'2016-2017','T6(1-3)', 10)
--hk1-nam nay
insert into SCHEDULEPOINT
values('1512123','CTT105',1,'2017-2018','T2(1-3)', null)
insert into SCHEDULEPOINT
values('1512123','CTT501',1,'2017-2018','T7(1-3)', null)
insert into SCHEDULEPOINT
values('1512123','CTT502',1,'2017-2018','T5(3-6)', null)
insert into SCHEDULEPOINT
values('1512123','TTH045',1,'2017-2018','T4(1-3)', null)

GO

-- SP dang nhap vao he thong
CREATE PROC USP_Login
@username varchar(10), @password varchar(20)
as
BEGIN
	SELECT * 
	FROM ACCOUNT 
	WHERE Username = @username COLLATE Latin1_General_CS_AS
	AND Password = @password COLLATE Latin1_General_CS_AS
END
go

-- SP kiem tra ID co hop le
CREATE PROC USP_IDvalidation
@username varchar(10)
as
BEGIN
	SELECT * 
	FROM ACCOUNT 
	WHERE Username = @username COLLATE Latin1_General_CS_AS
END
go

-- SP lay thong tin toan bo hoc sinh
CREATE PROC USP_GetInfoStudentList
AS SELECT IdStu as N'MSSV', Name as N'Tên',Sex as N'Giới tính', DoB as N'Ngày Sinh',PhoneNo as N'Điên Thoại',IDNo as 'CMND',Address as N'Địa Chỉ',Email FROM dbo.STUDENT
GO

-- SP lay thong tin cua mot sinh vien co MSSV tuong ung
CREATE PROC USP_GetInfoCurrentStudent
@IdStu varchar(10)
as
begin
	select * from STUDENT where IdStu = @IdStu
end
GO


-- SP doi mat khau
create proc USP_ChangePassword
@username varchar(10), @password varchar(20), @newpassword varchar(20)
as
begin
	update ACCOUNT set Password = @newpassword where Username = @username and Password = @password
end
go

create proc USP_ChangeInfo
@username varchar(10), @HoTen nvarchar(50), @GioiTinh nvarchar(5), @NgaySinh datetime, @SDT varchar(12), @CMND varchar(10), @DiaChi nvarchar(100), @Email varchar(50)
as
begin
	update STUDENT set Name=@HoTen,Sex=@GioiTinh,DoB=@NgaySinh,PhoneNo=@SDT,IDNo=@CMND,Address=@DiaChi,Email=@Email where IdStu = @username 
end
go

create proc USP_DeleteMember
@username varchar(10)
as
begin
	delete from ACCOUNT where Username=@username
	delete from SCHEDULEPOINT where IdStu=@username
	delete from STUDENT where IdStu=@username
end 
go

--------------------------------------

--lấy id để kiểm tra khi thêm
CREATE PROC USP_QueryId
@username varchar(10)
as
BEGIN
	SELECT * FROM ACCOUNT WHERE Username = @username
END
go

--lấy cmnd để kiểm tra khi thêm
CREATE PROC USP_QueryCMND
@idNo varchar(10)
as
BEGIN
	SELECT * FROM STUDENT WHERE IDNo = @idNo
END
go


--SP Them thong tin sinh vien
create proc USP_AddInfoStudent
@username varchar(10), @HoTen nvarchar(50),@GioiTinh nvarchar(5), @NgaySinh datetime, @SDT varchar(12), @CMND varchar(10), @DiaChi nvarchar(100), @Email varchar(50)
as
begin
	insert into STUDENT(IdStu, Name, Sex, DoB, PhoneNo, IDNo, Address, Email)
	values (@username, @HoTen, @GioiTinh, @NgaySinh, @SDT, @CMND, @DiaChi, @Email)
end
go
------------------------------------
-- SP lay bang diem

CREATE PROC USP_GetScore
@MSSV varchar(10), @Semester int, @Year varchar(15)
as
begin
	select SCHEDULEPOINT.idSub as N'Mã Môn Học', SUBJECTLIST.Name as N'Tên Môn Học',SCHEDULEPOINT.Semester as N'Học Kỳ', SCHEDULEPOINT.Year as N'Năm Học', SCHEDULEPOINT.Point as N'Điểm'
	from SCHEDULEPOINT, SUBJECTLIST 
	where SCHEDULEPOINT.idStu = @MSSV and SCHEDULEPOINT.Semester = @Semester and SCHEDULEPOINT.Year = @Year and SCHEDULEPOINT.idSub = SUBJECTLIST.IdSub
end
go
-- SP lay toan bo bang diem
ALTER PROC USP_GetScore_All
@MSSV varchar(10)
as
begin
	select SCHEDULEPOINT.idSub as N'Mã Môn Học', SUBJECTLIST.Name as N'Tên Môn Học',SCHEDULEPOINT.Semester as N'Học Kỳ', SCHEDULEPOINT.Year as N'Năm Học', SCHEDULEPOINT.Point as N'Điểm'
	from SCHEDULEPOINT, SUBJECTLIST 
	where SCHEDULEPOINT.idStu = @MSSV  and SCHEDULEPOINT.idSub = SUBJECTLIST.IdSub and SCHEDULEPOINT.Point is not null
end
go

-- SP check nam hoc va hoc ki
CREATE PROC USP_GetYear
@MSSV varchar(10)
as
begin
	select SCHEDULEPOINT.Year 
	from SCHEDULEPOINT 
	where SCHEDULEPOINT.idStu = @MSSV 
	group by SCHEDULEPOINT.Year
end
go

-- SP thay doi diem
CREATE PROC USP_ChangeScore
@MSSV varchar(10), @Semester int, @Year varchar(15), @IdSub varchar(10), @Point float
as
begin
	update SCHEDULEPOINT set Point = @Point where idStu = @MSSV and Year = @Year and Semester = @Semester and idSub = @IdSub
end
go

--SP Load danh sach mon chua nhap diem
create proc USP_LoadListUnregisteredSub
@MSSV varchar(10), @Semester int, @Year varchar(15)
as
begin
	select  SUBJECTLIST.Name 
	from SCHEDULEPOINT, SUBJECTLIST
	where SCHEDULEPOINT.idStu = @MSSV 
	and SCHEDULEPOINT.Year = @Year 
	and SCHEDULEPOINT.Semester = @Semester 
	and SUBJECTLIST.IdSub = SCHEDULEPOINT.idSub 
	and SCHEDULEPOINT.idSub not in (select SCHEDULEPOINT.idSub 
								from SCHEDULEPOINT 
								where SCHEDULEPOINT.idStu = @MSSV 
								and SCHEDULEPOINT.Year = @Year 
								and SCHEDULEPOINT.Semester = @Semester)
	group by  SUBJECTLIST.Name
end
go

--sp load danh sach sv va diem theo mon
create proc USP_LoadListStuSub
@IdSub varchar(10), @Semester int, @Year varchar(15)
as
begin
	select STUDENT.IdStu as 'Id', STUDENT.Name as N'Họ Tên', SCHEDULEPOINT.Point as N'Điểm'
	from SCHEDULEPOINT, STUDENT
	where SCHEDULEPOINT.idStu = STUDENT.IdStu  and SCHEDULEPOINT.idSub = @IdSub and SCHEDULEPOINT.Year = @Year and SCHEDULEPOINT.Semester = @Semester
end
go

-- Schedule
create proc USP_QuerySchedule
@username varchar(10),
@HK		int,
@year	varchar(15)
as
begin
	SELECT SUBJECTLIST.IdSub as N'Mã Môn Học',SUBJECTLIST.Name as N'Tên Môn Học',SCHEDULEPOINT.Period as N'Lịch Học' 
	FROM SCHEDULEPOINT, SUBJECTLIST 
	WHERE SCHEDULEPOINT.idStu=@username and SCHEDULEPOINT.idSub=SUBJECTLIST.IdSub and SCHEDULEPOINT.Semester=@HK and SCHEDULEPOINT.Year=@year
end
go

create proc USP_QueryScheduleSubject
@idSub varchar(10),
@HK		int,
@year	varchar(15)
as
begin
	SELECT SCHEDULEPOINT.idStu as N'Mã Học Sinh',SCHEDULEPOINT.Period as N'Lịch Học' 
	FROM SCHEDULEPOINT
	WHERE SCHEDULEPOINT.idSub=@idSub and  SCHEDULEPOINT.Semester=@HK and SCHEDULEPOINT.Year=@year
end
go

create proc USP_ListSemesterSub
@idSub varchar(10),
@year varchar(15)
as
begin
	select distinct SCHEDULEPOINT.Semester as 'Semester'
	from SCHEDULEPOINT
	where SCHEDULEPOINT.idSub=@idSub and SCHEDULEPOINT.Year=@year
end
go

create proc USP_ListSemesterStu
@idStu varchar(10),
@year varchar(15)
as
begin
	select distinct SCHEDULEPOINT.Semester as 'Semester'
	from SCHEDULEPOINT
	where SCHEDULEPOINT.idStu=@idStu and SCHEDULEPOINT.Year=@year
end
go

create proc USP_QueryYear
as SELECT DISTINCT Year as 'Year'
FROM dbo.SCHEDULEPOINT
go

create proc USP_CheckItemSchedule 
@idStd varchar(10),
@idSub varchar(10),
@HK		int,
@year	varchar(15)
as
begin
	SELECT SCHEDULEPOINT.idSub as 'IdSubject'
	FROM SCHEDULEPOINT 
	WHERE SCHEDULEPOINT.idStu=@idStd and SCHEDULEPOINT.idSub=@idSub and SCHEDULEPOINT.Semester=@HK and SCHEDULEPOINT.Year=@year
end
go

create proc USP_UpdateItemSchedule
@idStd varchar(10),
@idSub varchar(10),
@HK		int,
@year	varchar(15),
@period varchar(10)
as
begin
	UPDATE SCHEDULEPOINT Set Period=@period 
	where SCHEDULEPOINT.idStu=@idStd and SCHEDULEPOINT.idSub=@idSub and SCHEDULEPOINT.Semester=@HK and SCHEDULEPOINT.Year=@year
end
go

create proc USP_InsertItemSchedule
@idStd varchar(10),
@idSub varchar(10),
@HK		int,
@year	varchar(15),
@period varchar(10)
as
begin
	insert into SCHEDULEPOINT 
	values(@idStd,@idSub,@HK,@year,@period, null)

end
go

create proc USP_DeleteSchedule
@idStd varchar(10)
as
begin
	delete from SCHEDULEPOINT where SCHEDULEPOINT.idStu=@idStd
end
go

create proc USP_DeleteScheduleSubject
@idSub varchar(10)
as
begin
	delete from SCHEDULEPOINT where SCHEDULEPOINT.idSub=@idSub
end
go

--------------------------------
--Report
create proc USP_GetScoreBoardForReport
@IdStu varchar(10)
as
begin
	select stu.Name, sub.Name, point.Year, point.Semester, point.Point
	from STUDENT stu, SUBJECTLIST sub, SCHEDULEPOINT point
	where stu.IdStu = point.idStu
	and sub.IdSub = point.idSub
	and point.IdStu = @IdStu
	and point.Point is not null
end
go

--SP Them tai khoan sinh vien
create proc USP_AddAccount
@username varchar(10)
as
begin
	insert into ACCOUNT(Username,Password,type)
	values (@username,@username,0)
end
go

-- SP lay danh sach mon hoc

CREATE PROC USP_GetSubjectList

AS SELECT IdSub as N'MMH', Name as N'Tên Môn Học' FROM dbo.SUBJECTLIST

GO

--SP Them mon hoc

create proc USP_AddSubject
@idSub varchar(10), @Name nvarchar(50)

as

begin

	insert into SUBJECTLIST(IdSub, Name)

	values (@idSub,@Name)

end

go

--lấy id để kiểm tra khi thêm

CREATE PROC USP_QueryIdSub 

@idSub varchar(10)

as

BEGIN

	SELECT * FROM SUBJECTLIST WHERE IdSub = @idSub

END

go

--Tải môn học để xem trước trong form
CREATE PROC USP_GetInfoCurrentSubject

@IdSub varchar(10)

as

begin

	select * from SUBJECTLIST where IdSub = @IdSub

end

GO



--Xóa subject trong database 
create proc USP_DeleteSubject
@idSub varchar(10)
as
begin
	delete from SCHEDULEPOINT where idSub=@idSub
	delete from SUBJECTLIST where IdSub=@idSub
end 
go



--Đổi thông tin subject (tên môn học)


create proc USP_ChangeSubject

@idSub varchar(10),@name nvarchar(50)

as

begin
	update SUBJECTLIST set Name=@name where IdSub=@idSub

end

go

